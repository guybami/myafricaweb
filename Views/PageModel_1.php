 <?php
            
/**
 * PageModel
 * Class used to generate dynamic html pages
 * 
 * @author     Guy Bami
 * Last update: 13.02.13
*/
class PageModel {

    // class members with default values
    private $pageContent = "Welcome content";
    private $pageTitle = "Gestion Budget Maries";
    private $userLanguage = "fr-FR";  //default language culture
    // default language
    private $pageJscript;
    private $displayLanguageSelection = false;
    private $useDojoScripts = true;
    // by default the selection is done only on the index.php page
    private $directoryLevel = "..";
    private $menuLevel = ".";
    private $homeLevel = "../..";
    private $activeMenuCssArray = array('home' => 'activeMenuItem', 
                                                    'employees' => '', 
                                                    'projects' => '', 
                                                    'expenses' => '',
                                                    'missions' => '', 
                                                    'containers' => '', 
                                                    'users' => '', 
                                                    'administration' => '');
    private $dojoLibPath = "/Lib/dojo-release-1.8.3/";

    //Variable contenant la strucure du template
    public $DataForTemplate = "";
        
    //The constructor
    function PageModel() {
            
        $this->setUserCurrentLanguage();
        // default language
        $this->directoryLevel = "..";
        $this->menuLevel = ".";
        $this->homeLevel = "../..";
    }

    // class setters and getters functions
    function  setUserCurrentLanguage(){
        if (isset($_GET) && count($_GET) > 0 && $_GET['userLang']) {
            // no data sent
            $_SESSION['userLang'] = $_GET['userLang'];
        }  
        else if (!isset($_SESSION['userLang'])){
            // set default language culture
            $_SESSION['userLang'] = "fr-FR";
        } 
    }
    
    
    function setShouldDisplayLanguageSelection($flag) {
        $this->displayLanguageSelection = $flag;
    }

    function setTitle($newTitle) {
        $this->pageTitle = $newTitle;
    }

    function setPageJscript($script) {
        $this->pageJscript = $script;
    }

    function setContent($content) {
        $this->pageContent = $content;
    }

    function setUserLanguage($userLang) {
        $this->userLanguage = $userLang;
    }

    function setUseDojoScripts($useDojo) {
        $this->useDojoScripts = $useDojo;
    }

    function setDirectoryLevel($level) {
        if ($level == 0) {
            $this->directoryLevel = ".";
            $this->menuLevel = ".";
            $this->homeLevel = ".";
        }
        else if ($level == 1) {
            $this->directoryLevel = "..";
            $this->menuLevel = ".";
            $this->homeLevel = "..";
        } else if ($level == 2) {
            $this->directoryLevel = "../..";
            $this->menuLevel = "../..";
            $this->homeLevel = "../..";
        } 
    }

    function displayPage() {
        $this->displayHeader();
        $this->displayMainMenu();
        $this->displayContent();
        $this->displayFooter();
    }

    function displayHeader() {
        echo '
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <title>'.$this->pageTitle.'</title>
                        <!--include all CSS files-->
                        <link rel="stylesheet" type="text/css" href="' . $this->directoryLevel . '/Resources/Styles/master.css" />
                        <link rel="stylesheet" type="text/css" href="' . $this->directoryLevel . '/Resources/Styles/common.css" />
                        <link rel="stylesheet" type="text/css" href="' . $this->directoryLevel . '/Resources/Styles/controls.css" />
                        <link rel="stylesheet" type="text/css" href="' . $this->directoryLevel . '/Resources/Styles/mainMenu.css" />
                        <link rel="stylesheet" href="' . $this->directoryLevel . '/Lib/dojo-release-1.8.3/dijit/themes/claro/claro.css">
                        <link rel="stylesheet" href="' . $this->directoryLevel . '/Lib/dojo-release-1.8.3/dojox/grid/resources/claroGrid.css" type="text/css" />
                        <link rel="stylesheet" href="' . $this->directoryLevel . '/Lib/dojo-release-1.8.3/dojox/grid/enhanced/resources/claro/EnhancedGrid.css" type="text/css" />
                        <link rel="stylesheet" href="' . $this->directoryLevel . '/Lib/dojo-release-1.8.3/dojox/grid/enhanced/resources/EnhancedGrid_rtl.css" type="text/css" />
                        <link rel="stylesheet" type="text/css" href="' . $this->directoryLevel . '/Resources/Styles/jquery.dropdown.css" />
                        <link rel="stylesheet" type="text/css" href="' . $this->directoryLevel . '/Resources/Styles/dojoCustomWidgets.css" />
                        <link rel="stylesheet" type="text/css" href="' . $this->directoryLevel . '/Resources/Styles/customEffects.css" />
                        
                        <link rel="stylesheet" type="text/css" href="' . $this->directoryLevel . '/Resources/Images/flashAminations/engine0/style.css" />

                        <!--add scripts files-->
                        <script src="' . $this->directoryLevel . '/Scripts/jquery-1.8.3.js" type="text/javascript"></script>
                        <script src="' . $this->directoryLevel . '/Scripts/mainJScripts.js" type="text/javascript"></script>
                        <script src="' . $this->directoryLevel . '/Scripts/commonScript.js" type="text/javascript"></script>
                        <script src="' . $this->directoryLevel . '/Scripts/jquery.dropdown.js" type="text/javascript" ></script>
                        <script src="' . $this->directoryLevel . '/Scripts/customAnimationsScript.js" type="text/javascript" ></script>';


        if ($this->useDojoScripts) {
            echo '     
                  <!--add dojo scripts files-->
                  <script>
                    var dojoConfig = {parseOnLoad: false, isDebug:true, async: true, bindEncoding:"UTF-8", locale:"' . $this->getCurrentLocale() . '"};
                    var pageConfig = { useJqueryEffects : true, logToConsole : true, logLevel : 2 };
                  </script>';

            echo '
                  <!-- use local dojo scripts lib -->
                    <script src="' . $this->directoryLevel . $this->dojoLibPath . 'dojo/dojo.js" type="text/javascript"></script>
                  ';
            echo '
                  <script  type="text/javascript">
                      require(["dojo/parser", "dojo/ready"], function(parser, ready){
                        ready(function(){
                           setHtmlPageDimensions();
                        });
                      });
                  </script>';
        }
        else {
            // need of dojo lib
            echo '
                    <script  type="text/javascript">
                      $(document).ready(function () {
                          setHtmlPageDimensions();
                      });
                    </script>
                  ';
        }
        
        /// add customm page js script
        echo $this->pageJscript;
        echo '
            <!--[if IE 6]>  
                <style>  
                body {behavior: url("../Resources/Styles/csshover3.htc");}  
            </style>  
            <![endif]--> 
        </head>
        <body class="claro">';
    }

    function setActiveMenu($menuTitle) {
        switch ($menuTitle) {
            case "home":
                $this->activeMenuCssArray = array('home' => 'activeMenuItem', 
                                                    'employees' => '', 
                                                    'projects' => '', 
                                                    'expenses' => '',
                                                    'missions' => '', 
                                                    'containers' => '', 
                                                    'users' => '', 
                                                    'administration' => '');
                break;
            case "employees":
                $this->activeMenuCssArray = array('home' => '', 
                                                'employees' => 'activeMenuItem', 
                                                'projects' => '', 
                                                'expenses' => '',
                                                'missions' => '', 
                                                'containers' => '', 
                                                'users' => '', 
                                                'administration' => '');
                break;
            case "projects":
                $this->activeMenuCssArray = array('home' => '', 
                                                'employees' => '', 
                                                'projects' => 'activeMenuItem', 
                                                'expenses' => '',
                                                'missions' => '', 
                                                'containers' => '', 
                                                'users' => '', 
                                                'administration' => '');
                break;
            case "expenses":
                $this->activeMenuCssArray = array('home' => '', 
                                                'employees' => '', 
                                                'projects' => '', 
                                                'expenses' => 'activeMenuItem',
                                                'missions' => '', 
                                                'containers' => '', 
                                                'users' => '', 
                                                'administration' => '');
                break;
            case "missions":
                $this->activeMenuCssArray = array('home' => '', 
                                                'employees' => '', 
                                                'projects' => '', 
                                                'expenses' => '',
                                                'missions' => 'activeMenuItem', 
                                                'containers' => '', 
                                                'users' => '', 
                                                'administration' => '');
                break;
            case "containers":
                $this->activeMenuCssArray = array('home' => '', 
                                                'employees' => '', 
                                                'projects' => '', 
                                                'expenses' => '',
                                                'missions' => '', 
                                                'containers' => 'activeMenuItem', 
                                                'users' => '', 
                                                'administration' => '');
                break;
            case "users":
                $this->activeMenuCssArray = array('home' => '', 
                                                'employees' => '', 
                                                'projects' => '', 
                                                'expenses' => '',
                                                'missions' => '', 
                                                'containers' => '', 
                                                'users' => 'activeMenuItem', 
                                                'administration' => '');
                break;
            case "administration":
                $this->activeMenuCssArray = array('home' => '', 
                                                'employees' => '', 
                                                'projects' => '', 
                                                'expenses' => '',
                                                'missions' => '', 
                                                'containers' => '', 
                                                'users' => '', 
                                                'administration' => 'activeMenuItem');
                break;
            default:
                $this->activeMenuCssArray = array('home' => 'activeMenuItem', 
                                                'employees' => '', 
                                                'projects' => '', 
                                                'expenses' => '',
                                                'missions' => '', 
                                                'containers' => '', 
                                                'users' => '', 
                                                'administration' => '');
                break;
        }
    }

    function getCurrentLocale() {
        $userLang = $_SESSION['userLang'];
        $locale = "fr"; // default langauge
        if ($userLang == "fr-FR") {
            $locale = "fr";
        } else {
            $locale = "en";
        }
        return $locale;
    }

    function loadUIResource() {
        $userLang = $_SESSION['userLang'];
        if ($userLang == "fr-FR"){
            include_once "$this->homeLevel/UIResources/master.fr.res.php";
        }
        else{
            include_once "$this->homeLevel/UIResources/master.en.res.php";
        }
    }

    function displayMainMenu() {
        $liAdminStyle = "display:none;";
        $tableClass = "flagsTable";
        if (!$this->displayLanguageSelection) {
            $tableClass = "flagsTable hideContent";
        }
            
        $this->loadUIResource();
        
        if (isset($_SESSION) && User::isAdministrator($_SESSION['userID'])) {
            $liAdminStyle = ""; // the current user has admin rights access
        } 

        echo '
         <table class="leftRightBorder fullWidth pageBackColor" cellpadding="0" cellspacing="0" id="pageTable">
            <tr> 
                <td id="pageHeader">
                  <table class="fullWidth hideBorder" cellpadding="0" cellspacing="0">
                      <tr class="logoRow pageBackColor">
                          <td class="logoSite toCenter"  valign="top">  
                               <img id="siteLogoImg" alt="Gestion Budget Maries"  
                                    src="' . $this->homeLevel . '/Resources/Images/siteLogo.png" />
                          </td>
                          <td class="toLeft  titleRow" nowrap="nowrap" valign="middle">
                            <a href="' . $this->homeLevel . '"> 
                               <span class="titleRow">GESTION BUDGETS DES MAIRIES</span>
                            </a>
                          </td>
                          <td  align="right"   nowrap="nowrap">
                              <table cellspacing="2">
                                <tr>
                                    <td nowrap="nowrap">
                                        <div class="contactDiv">
                                            <span class="userNameLabel" id="userNameSpan" data-dropdown="#dropdown-1">'
                                                    . $this->getCurrentUserName() . '
                                                <img id="userProfileImgMenu"  src="' . $this->homeLevel . '/Resources/Images/drop.png" />
                                            </span>
                                            <span>|</span>
                                            <a class="sitemapLink" href="' . $this->homeLevel . '/ContactUs.php">' . MasterUIResource::getContactText() . '</a>
                                            <span class="sitemapLink">|</span>
                                            <a class="sitemapLink" href="' . $this->homeLevel . '/Sitemap.php">Sitemap</a>
                                        </div>
                                        <div id="dropdown-1" class="dropdown dropdown-tip">
                                            <ul class="dropdown-menu">
                                                <li><a href="' . $this->menuLevel . '/Users/UserProfile.php">' . MasterUIResource::getMyProfileText() . '</a></li>
                                                <li class="dropdown-divider"></li>
                                                <li><a href="' . $this->homeLevel . '/UserLogout.php">' . MasterUIResource::getLogoutText() . '</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="langSelectionRow">
                                    <td   class="pageBackColor" align="right">
                                        <table class="hideBorder hideContent ' . $tableClass . '" cellspacing="3">
                                            <tr>
                                                <td align="right" valign="top">
                                                    <table  class="fullWidth">
                                                        <tr>
                                                            <td class=" toCenter">
                                                                <a  title="Francais" class="flagLink flagImg frenchFlagImg"
                                                                    href="' . $this->homeLevel . '?userLang=fr-FR">Francais</a>
                                                            </td>
                                                            <td class="toCenter">
                                                                <a href="' . $this->homeLevel . '?userLang=en-US"
                                                                    title="English" class="flagLink flagImg englishFlagImg">English</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                             </table>
                           </td>
                        </tr>
                        <tr>
                             <td align="center" colspan="3" class="fullWidth">
                                <!-- start main menu -->
                                 <div class="menuContainer">
                                    <div class="menuDiv" >
                                        <ul>
                                            <li class="firstMenuItem">
                                                <a href="' . $this->homeLevel . '" class="' . $this->activeMenuCssArray['home'] . '" >
                                                    <span class="">HOME</span>
                                                </a>
                                            </li>
                                            <li id="menuEmployees" class="menuItem">
                                                <a href="#" class="' . $this->activeMenuCssArray['employees'] . '">
                                                    <span>' . MasterUIResource::getMiEmployeesText() . '</span>
                                                </a>
                                                <ul>
                                                    <li id="miAllEmployes" class="lastItem">
                                                        <a href="' . $this->menuLevel . '/PresentationLayer/Employees/ViewAllEmployees.php">
                                                            <span>' . MasterUIResource::getSubMiEmployeesListText() . '</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li id="menuProjects" class="menuItem"> 
                                                <a href="#" class="' . $this->activeMenuCssArray['projects'] . '">
                                                    <span>' . MasterUIResource::getMiProjectsText() . '</span> 
                                                </a>
                                                <ul>
                                                    <li id="miAllProjects">
                                                        <a href="' . $this->menuLevel . '/PresentationLayer/Projects/ViewAllProjects.php">
                                                            <span>' . MasterUIResource::getSubMiProjectsListText() . '</span>
                                                        </a>
                                                    </li>
                                                    <li class="lastItem" id="miProjectGantt">
                                                        <a href="' . $this->menuLevel . '/PresentationLayer/Projects/ViewProjectsGanntChart.php">
                                                            <span>Diagrammes des Evolutions</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li id="menuExpenses" class="menuItem">
                                                <a href="#" class="' . $this->activeMenuCssArray['expenses'] . '">
                                                    <span>' . MasterUIResource::getMiExpensesText() . '</span> 
                                                </a>
                                                <ul>
                                                    <li  id="miAllExpenses">
                                                        <a href="' . $this->menuLevel . '/PresentationLayer/Expenses/ViewAllExpenses.php">
                                                            <span>' . MasterUIResource::getSubMiExpensesListText() . '</span></a>
                                                    </li>
                                                    <li class="lastItem" id="miDiagramExpenses">
                                                        <a href="' . $this->menuLevel . '/PresentationLayer/Expenses/ViewExpensesChart.php">
                                                            <span>Statistiques annuelles</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>

                                            <li id="menuMissions" class="menuItem"> 
                                                <a href="#" class="' . $this->activeMenuCssArray['missions'] . '">
                                                    <span>' . MasterUIResource::getMiMissionsText() . '</span> 
                                                </a>
                                                <ul>
                                                    <li id="miAllMissions" class="lastItem">
                                                        <a href="' . $this->menuLevel . '/PresentationLayer/Missions/ViewAllMissions.php">
                                                            <span>' . MasterUIResource::getSubMiMissionsListText() . '</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li id="menuUsers" class="lastMenuItem"> 
                                                <a href="" class="' . $this->activeMenuCssArray['users'] . '">
                                                    <span>' . MasterUIResource::getMiUsersText() . '</span> 
                                                </a>
                                                <ul>
                                                    <li id="miAllUsers">
                                                        <a href="' . $this->menuLevel . '/PresentationLayer/Users/ViewAllUsers.php">
                                                            <span>' . MasterUIResource::getSubMiUsersListText() . '</span>
                                                        </a>
                                                    </li>
                                                    <li id="miAllRoles" class="lastItem">
                                                        <a href="' . $this->menuLevel . '/PresentationLayer/Users/ViewAllRoles.php">
                                                            <span>Liste des Roles</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <!-- end main menu -->
                              </td>
                        </tr>
                    </table>
                </td>
            </tr>';
    }

    function displayContent() {
        echo '
            <tr>
                <td class="toCenter toTop" id="pageContent">
                    ' . $this->pageContent . '
                </td>
            </tr>';
    }

    function displayFooter() {
        echo '
                    <tr>
                        <td  align="right" class="topBorder">
                            <table cellspacing="10" cellpadding="2" class="hideBorder">
                                <tr class="impressumRow">
                                    <td class="toCenter">
                                        <span><a href="' . $this->homeLevel 
                                            . '" class="siteLink">Copyright &copy; 2013 - ' 
                                            . date("Y") . '  GESTION BUDGETS DES MAIRIES  </a> </span> 
                                    </td>
                                    <td class="toCenter">
                                        <span><a href="' . $this->homeLevel . '/Impressum.php" class="impressumLink">Impressum</a></span>
                                    </td>
                                </tr> 
                            </table>
                        </td>
                    </tr>
                </table>
            </body>
        </html>';
        ob_flush();
    }

    function getCurrentUserID() {
        //Utils::checkUserSession();
        if ($_SESSION['userID'] != null && isset($_SESSION['userID'])) {
            return $_SESSION['userID'];
        }
        return "-1";
    }

    function getCurrentUserName() {
        //Utils::checkUserSession();
        if ($_SESSION['userName'] != null && isset($_SESSION['userName'])) {
            return $_SESSION['userName'];
        }
        return "";
    }
    
            
    
            

}

            