-- phpMyAdmin SQL Dump
-- version 3.5.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 06, 2017 at 11:23 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


 
--
-- Database: `mAev`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllConferences`()
BEGIN
                DELETE FROM mAev_Conference;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllEventBillSummaries`()
BEGIN
                DELETE FROM mAev_EventBillSummary;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllEventPhotoComments`()
BEGIN
                DELETE FROM mAev_EventPhotoComment;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllEventPhotos`()
BEGIN
                DELETE FROM mAev_EventPhoto;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllEvents`()
BEGIN
                DELETE FROM mAev_Event;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllEventVideoComments`()
BEGIN
                DELETE FROM mAev_EventVideoComment;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllEventVideos`()
BEGIN
                DELETE FROM mAev_EventVideo;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllExpenses`()
BEGIN
                DELETE FROM mAev_Expense;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllLogActivities`()
BEGIN
                DELETE FROM mAev_LogActivity;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllMemberFees`()
BEGIN
                DELETE FROM mAev_MemberFee;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllMembers`()
BEGIN
                DELETE FROM mAev_Member;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllOfficeMembers`()
BEGIN
                DELETE FROM mAev_OfficeMember;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllOldExams`()
BEGIN
                DELETE FROM mAev_OldExam;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllProjects`()
BEGIN
                DELETE FROM mAev_Project;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllPublications`()
BEGIN
                DELETE FROM mAev_Publication;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllRoleAccessRights`()
BEGIN
                DELETE FROM mAev_RoleAccessRight;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllRoles`()
BEGIN
                DELETE FROM mAev_Role;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllTutorials`()
BEGIN
                DELETE FROM mAev_Tutorial;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllUserProfiles`()
BEGIN
                DELETE FROM mAev_UserProfile;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllUsers`()
BEGIN
                DELETE FROM mAev_User;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAllVeterans`()
BEGIN
                DELETE FROM mAev_Veteran;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteConference`(_conferenceId  int(11)  
)
BEGIN

                DELETE FROM mAev_Conference
                WHERE  conferenceId = _conferenceId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteEvent`(_eventId  int(11)  
)
BEGIN

                DELETE FROM mAev_Event
                WHERE  eventId = _eventId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteEventBillSummary`(_billSummaryId  int(11)  
)
BEGIN

                DELETE FROM mAev_EventBillSummary
                WHERE  billSummaryId = _billSummaryId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteEventPhoto`(_photoId  int(11)  
)
BEGIN

                DELETE FROM mAev_EventPhoto
                WHERE  photoId = _photoId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteEventPhotoComment`(_photoCommentId  int(11)  
)
BEGIN

                DELETE FROM mAev_EventPhotoComment
                WHERE  photoCommentId = _photoCommentId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteEventVideo`(_videoId  int(11)  
)
BEGIN

                DELETE FROM mAev_EventVideo
                WHERE  videoId = _videoId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteEventVideoComment`(_videoCommentId  int(11)  
)
BEGIN

                DELETE FROM mAev_EventVideoComment
                WHERE  videoCommentId = _videoCommentId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteExpense`(_expenseId  int(11)  
)
BEGIN

                DELETE FROM mAev_Expense
                WHERE  expenseId = _expenseId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteIncome`(_incomeId  int(11)  
)
BEGIN

                DELETE FROM mAev_Income
                WHERE  incomeId = _incomeId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteLogActivity`(_activityId  int(11)  
)
BEGIN

                DELETE FROM mAev_LogActivity
                WHERE  activityId = _activityId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteMember`(_memberId  int(11)  
)
BEGIN

                DELETE FROM mAev_Member
                WHERE  memberId = _memberId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteMemberFee`(_memberFeeId  int(11)  
)
BEGIN

                DELETE FROM mAev_MemberFee
                WHERE  memberFeeId = _memberFeeId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteOfficeMember`(IN `_officeMemberId` INT(11))
BEGIN

                DELETE FROM mAev_OfficeMember
                WHERE  officeMemberId = _officeMemberId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteOldExam`(_examId  int(11)  
)
BEGIN

                DELETE FROM mAev_OldExam
                WHERE  examId = _examId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteProject`(_projectId  int(11)  
)
BEGIN

                DELETE FROM mAev_Project
                WHERE  projectId = _projectId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deletePublication`(_publicationId  int(11)  
)
BEGIN

                DELETE FROM mAev_Publication
                WHERE  publicationId = _publicationId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteRole`(_roleId  int(11)  
)
BEGIN

                DELETE FROM mAev_Role
                WHERE  roleId = _roleId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteRoleAccessRight`(_roleAccessId  int(11)  
)
BEGIN

                DELETE FROM mAev_RoleAccessRight
                WHERE  roleAccessId = _roleAccessId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteTutorial`(_tutorialId  int(11)  
)
BEGIN

                DELETE FROM mAev_Tutorial
                WHERE  tutorialId = _tutorialId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteUser`(_userId  int(11)  
)
BEGIN

                DELETE FROM mAev_User
                WHERE  userId = _userId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteUserProfile`(_profileId  int(11)  
)
BEGIN

                DELETE FROM mAev_UserProfile
                WHERE  profileId = _profileId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteVeteran`(_veteranId  int(11)  
)
BEGIN

                DELETE FROM mAev_Veteran
                WHERE  veteranId = _veteranId;

            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewConference`(_date  datetime  
,_title  varchar(50)  
,_location  varchar(50)  
,_summary  text  
)
BEGIN
                INSERT INTO mAev_Conference (
                    date  
,title  
,location  
,summary  

                )
                VALUES (
                    _date  
,_title  
,_location  
,_summary  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewEvent`(_title  varchar(50)  
,_category  enum('CultureWeek','FirstSemesterParty','GalaNight','Gaduation','GrillParty','Challenge','Mourning','Divers','Football','Tournament')  
,_date  datetime  
,_location  varchar(50)  
,_summary  text  
)
BEGIN
                INSERT INTO mAev_Event (
                    title  
,category  
,date  
,location  
,summary  

                )
                VALUES (
                    _title  
,_category  
,_date  
,_location  
,_summary  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewEventBillSummary`(_eventId  int(11)  
,_title  varchar(50)  
,_summary  text  
,_summaryFileName  varchar(512)  
)
BEGIN
                INSERT INTO mAev_EventBillSummary (
                    eventId  
,title  
,summary  
,summaryFileName  

                )
                VALUES (
                    _eventId  
,_title  
,_summary  
,_summaryFileName  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewEventPhoto`(_eventId  int(11)  
,_fileFullName  varchar(512)  
,_title  varchar(50)  
)
BEGIN
                INSERT INTO mAev_EventPhoto (
                    eventId  
,fileFullName  
,title  

                )
                VALUES (
                    _eventId  
,_fileFullName  
,_title  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewEventPhotoComment`(IN `_photoId` INT(11), IN `_userId` INT(11), IN `_commentText` TEXT, IN `_date` DATETIME)
BEGIN
                INSERT INTO mAev_EventPhotoComment (
                    photoId  
,userId  
,commentText  
,date  

                )
                VALUES (
                    _photoId  
,_userId  
,_commentText  
,_date  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewEventVideo`(_eventId  int(11)  
,_fileFullName  varchar(512)  
,_title  varchar(50)  
)
BEGIN
                INSERT INTO mAev_EventVideo (
                    eventId  
,fileFullName  
,title  

                )
                VALUES (
                    _eventId  
,_fileFullName  
,_title  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewEventVideoComment`(_videoId  int(11)  
,_userId  int(11)  
,_commentText  text  
,_date  datetime  
)
BEGIN
                INSERT INTO mAev_EventVideoComment (
                    videoId  
,userId  
,commentText  
,date  

                )
                VALUES (
                    _videoId  
,_userId  
,_commentText  
,_date  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewExpense`(_eventId  int(11)  
,_title  varchar(50)  
,_amount  double  
,_category  enum('CultureWeek','FirstSemesterParty','GalaNight','Gaduation','GrillParty','Challenge','Mourning','Donation','Sport','Divers')  
,_billFileName  varchar(512)  
,_transactionDate  datetime  
)
BEGIN
                INSERT INTO mAev_Expense (
                    eventId  
,title  
,amount  
,category  
,billFileName  
,transactionDate  

                )
                VALUES (
                    _eventId  
,_title  
,_amount  
,_category  
,_billFileName  
,_transactionDate  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewIncome`(_eventId  int(11)  
,_title  varchar(50)  
,_amount  double  
,_category  enum('CultureWeek','FirstSemesterParty','GalaNight','Gaduation','GrillParty','Challenge','Mourning','Donation','Sport','Divers')  
,_billFileName  varchar(512)  
,_transactionDate  datetime  
)
BEGIN
                INSERT INTO mAev_Income (
                    eventId  
,title  
,amount  
,category  
,billFileName  
,transactionDate  

                )
                VALUES (
                    _eventId  
,_title  
,_amount  
,_category  
,_billFileName  
,_transactionDate  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewLogActivity`(_userId  int(11)  
,_summary  text  
,_date  datetime  
)
BEGIN
                INSERT INTO mAev_LogActivity (
                    userId  
,summary  
,date  

                )
                VALUES (
                    _userId  
,_summary  
,_date  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewMember`(_gender  enum('Male','Female')  
,_name  varchar(50)  
,_lastName  varchar(50)  
,_email  varchar(100)  
,_phoneNumber  varchar(50)  
,_zipCode  varchar(10)  
,_city  varchar(50)  
,_address  varchar(256)  
,_position  varchar(50)  
)
BEGIN
                INSERT INTO mAev_Member (
                    gender  
,name  
,lastName  
,email  
,phoneNumber  
,zipCode  
,city  
,address  
,position  

                )
                VALUES (
                    _gender  
,_name  
,_lastName  
,_email  
,_phoneNumber  
,_zipCode  
,_city  
,_address  
,_position  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewMemberFee`(_memberId  int(11)  
,_amount  double  
,_billFileName  varchar(512)  
,_transactionDate  datetime  
)
BEGIN
                INSERT INTO mAev_MemberFee (
                    memberId  
,amount  
,billFileName  
,transactionDate  

                )
                VALUES (
                    _memberId  
,_amount  
,_billFileName  
,_transactionDate  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewOfficeMember`(_memberId  int(11)  
,_position  enum('President','Secretary','ChiefCulture','ChiefSport','Treasurer')  
)
BEGIN
                INSERT INTO mAev_OfficeMember (
                    memberId  
,position  

                )
                VALUES (
                    _memberId  
,_position  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewOldExam`(_userId  int(11)  
,_subject  varchar(50)  
,_title  varchar(120)  
,_semester  varchar(10)  
,_date  datetime  
,_fileFullName  varchar(512)  
)
BEGIN
                INSERT INTO mAev_OldExam (
                    userId  
,subject  
,title  
,semester  
,date  
,fileFullName  

                )
                VALUES (
                    _userId  
,_subject  
,_title  
,_semester  
,_date  
,_fileFullName  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewProject`(_userId  int(11)  
,_title  varchar(50)  
,_summary  text  
)
BEGIN
                INSERT INTO mAev_Project (
                    userId  
,title  
,summary  

                )
                VALUES (
                    _userId  
,_title  
,_summary  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewPublication`(_userId  int(11)  
,_category  enum('Info','Concert','Hiwi','Mourning')  
,_summary  text  
,_date  datetime  
)
BEGIN
                INSERT INTO mAev_Publication (
                    userId  
,category  
,summary  
,date  

                )
                VALUES (
                    _userId  
,_category  
,_summary  
,_date  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewRole`(_name  varchar(50)  
,_description  varchar(120)  
)
BEGIN
                INSERT INTO mAev_Role (
                    name  
,description  

                )
                VALUES (
                    _name  
,_description  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewRoleAccessRight`(_roleId  int(11)  
,_entityType  varchar(50)  
,_createRight  bit(1)  
,_readRight  bit(1)  
,_editRight  bit(1)  
,_deleteRight  bit(1)  
,_fullRight  bit(1)  
)
BEGIN
                INSERT INTO mAev_RoleAccessRight (
                    roleId  
,entityType  
,createRight  
,readRight  
,editRight  
,deleteRight  
,fullRight  

                )
                VALUES (
                    _roleId  
,_entityType  
,_createRight  
,_readRight  
,_editRight  
,_deleteRight  
,_fullRight  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewTutorial`(_memberId  int(11)  
,_date  datetime  
,_subject  varchar(50)  
,_level  varchar(50)  
,_location  varchar(50)  
,_shedules  varchar(50)  
,_status  enum('Cancelled','Active')  
)
BEGIN
                INSERT INTO mAev_Tutorial (
                    memberId  
,date  
,subject  
,level  
,location  
,shedules  
,status  

                )
                VALUES (
                    _memberId  
,_date  
,_subject  
,_level  
,_location  
,_shedules  
,_status  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewUser`(_loginName  varchar(50)  
,_hashPassword  varchar(20)  
,_name  varchar(50)  
,_lastName  varchar(50)  
,_phoneNumber  varchar(50)  
,_email  varchar(100)  
)
BEGIN
                INSERT INTO mAev_User (
                    loginName  
,hashPassword  
,name  
,lastName  
,phoneNumber  
,email  

                )
                VALUES (
                    _loginName  
,_hashPassword  
,_name  
,_lastName  
,_phoneNumber  
,_email  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewUserProfile`(_userId  int(11)  
,_gender  enum('Male','Female')  
,_photoFileName  varchar(256)  
,_street  varchar(50)  
,_zipCode  varchar(10)  
,_city  varchar(50)  
,_address  varchar(256)  
,_defalutLanguage  enum('DE','FR','EN')  
)
BEGIN
                INSERT INTO mAev_UserProfile (
                    userId  
,gender  
,photoFileName  
,street  
,zipCode  
,city  
,address  
,defalutLanguage  

                )
                VALUES (
                    _userId  
,_gender  
,_photoFileName  
,_street  
,_zipCode  
,_city  
,_address  
,_defalutLanguage  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewVeteran`(_gender  enum('Male','Female')  
,_name  varchar(50)  
,_lastName  varchar(50)  
,_email  varchar(100)  
,_phoneNumber  varchar(50)  
,_zipCode  varchar(10)  
,_city  varchar(50)  
,_address  varchar(256)  
,_position  varchar(50)  
)
BEGIN
                INSERT INTO mAev_Veteran (
                    gender  
,name  
,lastName  
,email  
,phoneNumber  
,zipCode  
,city  
,address  
,position  

                )
                VALUES (
                    _gender  
,_name  
,_lastName  
,_email  
,_phoneNumber  
,_zipCode  
,_city  
,_address  
,_position  

                );
                SELECT LAST_INSERT_ID();
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectActualFinancesStatus`()
BEGIN
    SELECT (SELECT SUM(amount) FROM mAev_Income) As sumIncomes, 
    (SELECT SUM(amount) FROM mAev_Expense) As sumExpenses, 
    (SELECT SUM(amount) FROM mAev_Income) - (SELECT SUM(amount) FROM mAev_Expense)  AS actualAmount;
	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllConferences`()
BEGIN
                SELECT mAev_Conference.*
                FROM mAev_Conference    ;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllEntitiesRecordsCount`()
BEGIN
    SELECT table_name AS tableName, table_rows tableRows 
	FROM information_schema.tables 
	WHERE  table_schema = 'khev';
	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllEventBillSummaries`()
BEGIN
                SELECT mAev_EventBillSummary.*
                FROM mAev_EventBillSummary    ;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllEventPhotoComments`()
BEGIN
                SELECT mAev_EventPhotoComment.*,mAev_PhotoVideo.*,mAev_User.*
                FROM mAev_EventPhotoComment   LEFT JOIN mAev_PhotoVideo ON  mAev_EventPhotoComment.photoId = mAev_PhotoVideo.photoId  
 LEFT JOIN mAev_User ON  mAev_EventPhotoComment.userId = mAev_User.userId  
   ORDER BY mAev_EventPhotoComment.date  DESC;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllEventPhotos`()
BEGIN
   SELECT mAev_EventPhoto.*, mAev_Event.title AS eventTitle
    FROM mAev_EventPhoto 
	LEFT JOIN  mAev_Event 
	ON  mAev_Event.eventId =  mAev_EventPhoto.eventId
	ORDER BY mAev_Event.date  DESC;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllEvents`()
BEGIN
    SELECT mAev_Event.*,
    COUNT(mAev_EventPhoto.photoId) AS numPhotos
    FROM mAev_Event    
    LEFT JOIN mAev_EventPhoto
    ON mAev_EventPhoto.eventId = mAev_Event.eventId
    GROUP BY mAev_Event.eventId
    ORDER BY mAev_Event.date DESC;
                
	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllEventVideoComments`()
BEGIN
                SELECT mAev_EventVideoComment.*,mAev_EventVideo.*,mAev_User.*
                FROM mAev_EventVideoComment   LEFT JOIN mAev_EventVideo ON  mAev_EventVideoComment.videoId = mAev_EventVideo.videoId  
 LEFT JOIN mAev_User ON  mAev_EventVideoComment.userId = mAev_User.userId  
   ORDER BY mAev_EventVideoComment.date  DESC;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllEventVideos`()
BEGIN
                SELECT mAev_EventVideo.*,mAev_Event.*
                FROM mAev_EventVideo   LEFT JOIN mAev_Event ON  mAev_EventVideo.eventId = mAev_Event.eventId  
  ;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllExpenses`()
BEGIN
    SELECT mAev_Expense.*, mAev_Event.title AS eventTitle
    FROM mAev_Expense 
	LEFT JOIN  mAev_Event 
	ON  mAev_Event.eventId =  mAev_Expense.eventId
	ORDER BY mAev_Expense.transactionDate  DESC;         
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllExpensesByYear`()
BEGIN
    SELECT 
        SUM(amount) AS sumExpenses,  
        MONTH(transactionDate) AS currentMonth,
        YEAR(transactionDate) AS currentYear
        FROM mAev_Expense   
        GROUP BY  MONTH(transactionDate), YEAR(transactionDate)
        ORDER BY  YEAR(transactionDate), MONTH( transactionDate);  
                
	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllIncomes`()
BEGIN
    SELECT mAev_Income.*, mAev_Event.title AS eventTitle
    FROM mAev_Income 
	LEFT JOIN  mAev_Event 
	ON  mAev_Event.eventId =  mAev_Income.eventId
	ORDER BY mAev_Income.transactionDate  DESC;
	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllIncomesByYear`()
BEGIN
    SELECT 
        SUM(amount) AS sumIncomes,  
        MONTH(transactionDate) AS currentMonth,
        YEAR(transactionDate) AS currentYear
        FROM mAev_Income   
        GROUP BY  MONTH(transactionDate), YEAR(transactionDate)
        ORDER BY  YEAR(transactionDate), MONTH( transactionDate);   
                
	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllLogActivities`()
BEGIN
                SELECT mAev_LogActivity.*,mAev_User.*
                FROM mAev_LogActivity   LEFT JOIN mAev_User ON  mAev_LogActivity.userId = mAev_User.userId  
   ORDER BY mAev_LogActivity.date  DESC;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllMemberFees`()
BEGIN
                SELECT mAev_MemberFee.*,mAev_Member.*
                FROM mAev_MemberFee   LEFT JOIN mAev_Member ON  mAev_MemberFee.memberId = mAev_Member.memberId  
  ;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllMembers`()
BEGIN
                SELECT mAev_Member.*
                FROM mAev_Member     ORDER BY mAev_Member.name  ASC;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllOfficeMembers`()
BEGIN
                SELECT mAev_OfficeMember.*,mAev_Member.*
                FROM mAev_OfficeMember   LEFT JOIN mAev_Member ON  mAev_OfficeMember.memberId = mAev_Member.memberId  
  ;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllOldExams`()
BEGIN
                SELECT mAev_OldExam.*,mAev_User.*
                FROM mAev_OldExam   LEFT JOIN mAev_User ON  mAev_OldExam.userId = mAev_User.userId  
   ORDER BY mAev_OldExam.title  ASC;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllPhotosByEvent`(IN `_eventId` INT(11))
BEGIN
    SELECT mAev_EventPhoto.*, mAev_Event.title AS eventTitle
    FROM mAev_EventPhoto 
    LEFT JOIN  mAev_Event 
    ON  mAev_Event.eventId =  mAev_EventPhoto.eventId
    WHERE mAev_EventPhoto.eventId = _eventId
    ORDER BY mAev_Event.date  DESC;
	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllProjects`()
BEGIN
                SELECT mAev_Project.*,mAev_User.*
                FROM mAev_Project   LEFT JOIN mAev_User ON  mAev_Project.userId = mAev_User.userId  
   ORDER BY mAev_Project.title  ASC;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllPublications`()
BEGIN
                SELECT mAev_Publication.*,mAev_User.*
                FROM mAev_Publication   LEFT JOIN mAev_User ON  mAev_Publication.userId = mAev_User.userId  
   ORDER BY mAev_Publication.date DESC;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllRoleAccessRights`()
BEGIN
                SELECT mAev_RoleAccessRight.*,mAev_Role.*
                FROM mAev_RoleAccessRight   LEFT JOIN mAev_Role ON  mAev_RoleAccessRight.roleId = mAev_Role.roleId  
  ;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllRoles`()
BEGIN
                SELECT mAev_Role.*
                FROM mAev_Role    ;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllTutorials`()
BEGIN
                SELECT mAev_Tutorial.*
                FROM mAev_Tutorial    ;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllUserProfiles`()
BEGIN
                SELECT mAev_UserProfile.*,mAev_User.*
                FROM mAev_UserProfile   LEFT JOIN mAev_User ON  mAev_UserProfile.userId = mAev_User.userId  
  ;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllUsers`()
BEGIN
                SELECT mAev_User.*
                FROM mAev_User     ORDER BY mAev_User.name  ASC;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectAllVeterans`()
BEGIN
                SELECT mAev_Veteran.*
                FROM mAev_Veteran    ;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectConferenceDetails`(_conferenceId  int(11)  
)
BEGIN
                SELECT * FROM mAev_Conference
                
                WHERE
                        conferenceId = _conferenceId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectEventBillSummaryDetails`(_billSummaryId  int(11)  
)
BEGIN
                SELECT * FROM mAev_EventBillSummary
                
                WHERE
                        billSummaryId = _billSummaryId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectEventDetails`(_eventId  int(11)  
)
BEGIN
                SELECT * FROM mAev_Event
                
                WHERE
                        eventId = _eventId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectEventPhotoCommentDetails`(_photoCommentId  int(11)  
)
BEGIN
                SELECT * FROM mAev_EventPhotoComment
                 LEFT JOIN mAev_PhotoVideo ON  mAev_EventPhotoComment.photoId = mAev_PhotoVideo.photoId  
 LEFT JOIN mAev_User ON  mAev_EventPhotoComment.userId = mAev_User.userId  

                WHERE
                        photoCommentId = _photoCommentId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectEventPhotoDetails`(_photoId  int(11)  
)
BEGIN
                SELECT * FROM mAev_EventPhoto
                 LEFT JOIN mAev_Event ON  mAev_EventPhoto.eventId = mAev_Event.eventId  

                WHERE
                        photoId = _photoId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectEventsByCategory`(IN `_category` 
     enum('CultureWeek','FirstSemesterParty','GalaNight','Gaduation','GrillParty','Challenge','Mourning','Divers','Football','Tournament')  )
BEGIN
    SELECT mAev_Event.*,
    COUNT(mAev_EventPhoto.photoId) AS numPhotos
    FROM mAev_Event    
    LEFT JOIN mAev_EventPhoto
    ON mAev_EventPhoto.eventId = mAev_Event.eventId
    WHERE mAev_Event.category = _category
    GROUP BY mAev_Event.eventId
    ORDER BY mAev_Event.date DESC;
                
	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectEventVideoCommentDetails`(_videoCommentId  int(11)  
)
BEGIN
                SELECT * FROM mAev_EventVideoComment
                 LEFT JOIN mAev_EventVideo ON  mAev_EventVideoComment.videoId = mAev_EventVideo.videoId  
 LEFT JOIN mAev_User ON  mAev_EventVideoComment.userId = mAev_User.userId  

                WHERE
                        videoCommentId = _videoCommentId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectEventVideoDetails`(_videoId  int(11)  
)
BEGIN
                SELECT * FROM mAev_EventVideo
                 LEFT JOIN mAev_Event ON  mAev_EventVideo.eventId = mAev_Event.eventId  

                WHERE
                        videoId = _videoId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectExpenseDetails`(_expenseId  int(11)  
)
BEGIN
                SELECT * FROM mAev_Expense
                
                WHERE
                        expenseId = _expenseId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectIncomeDetails`(_incomeId  int(11)  
)
BEGIN
                SELECT * FROM mAev_Income
                
                WHERE
                        incomeId = _incomeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectLogActivityDetails`(_activityId  int(11)  
)
BEGIN
                SELECT * FROM mAev_LogActivity
                 LEFT JOIN mAev_User ON  mAev_LogActivity.userId = mAev_User.userId  

                WHERE
                        activityId = _activityId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectMemberDetails`(_memberId  int(11)  
)
BEGIN
                SELECT * FROM mAev_Member
                
                WHERE
                        memberId = _memberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectMemberFeeDetails`(_memberFeeId  int(11)  
)
BEGIN
                SELECT * FROM mAev_MemberFee
                 LEFT JOIN mAev_Member ON  mAev_MemberFee.memberId = mAev_Member.memberId  

                WHERE
                        memberFeeId = _memberFeeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectOfficeMemberDetails`(_officeMemberId  int(11)  
)
BEGIN
                SELECT * FROM mAev_OfficeMember
                 LEFT JOIN mAev_Member ON  mAev_OfficeMember.memberId = mAev_Member.memberId  

                WHERE
                        officeMemberId = _officeMemberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectOldExamDetails`(_examId  int(11)  
)
BEGIN
                SELECT * FROM mAev_OldExam
                 LEFT JOIN mAev_User ON  mAev_OldExam.userId = mAev_User.userId  

                WHERE
                        examId = _examId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectProjectDetails`(_projectId  int(11)  
)
BEGIN
                SELECT * FROM mAev_Project
                 LEFT JOIN mAev_User ON  mAev_Project.userId = mAev_User.userId  

                WHERE
                        projectId = _projectId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectPublicationDetails`(_publicationId  int(11)  
)
BEGIN
                SELECT * FROM mAev_Publication
                 LEFT JOIN mAev_User ON  mAev_Publication.userId = mAev_User.userId  

                WHERE
                        publicationId = _publicationId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectRoleAccessRightDetails`(_roleAccessId  int(11)  
)
BEGIN
                SELECT * FROM mAev_RoleAccessRight
                 LEFT JOIN mAev_Role ON  mAev_RoleAccessRight.roleId = mAev_Role.roleId  

                WHERE
                        roleAccessId = _roleAccessId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectRoleDetails`(_roleId  int(11)  
)
BEGIN
                SELECT * FROM mAev_Role
                
                WHERE
                        roleId = _roleId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectTutorialDetails`(_tutorialId  int(11)  
)
BEGIN
                SELECT * FROM mAev_Tutorial
                
                WHERE
                        tutorialId = _tutorialId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectUserDetails`(_userId  int(11)  
)
BEGIN
                SELECT * FROM mAev_User
                
                WHERE
                        userId = _userId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectUserProfileDetails`(_profileId  int(11)  
)
BEGIN
                SELECT * FROM mAev_UserProfile
                 LEFT JOIN mAev_User ON  mAev_UserProfile.userId = mAev_User.userId  

                WHERE
                        profileId = _profileId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectVeteranDetails`(_veteranId  int(11)  
)
BEGIN
                SELECT * FROM mAev_Veteran
                
                WHERE
                        veteranId = _veteranId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateConference`(
                            _conferenceId  int(11)  
,_date  datetime  
,_title  varchar(50)  
,_location  varchar(50)  
,_summary  text  

                  )
BEGIN
                UPDATE  mAev_Conference
                SET     date =  _date 
,title =  _title 
,location =  _location 
,summary =  _summary 

                WHERE
                        conferenceId = _conferenceId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateConferenceDate`(
                _conferenceId  int(11)  
,_date  datetime  

            )
BEGIN
                UPDATE  mAev_Conference
                SET     date =  _date 

                WHERE
                        conferenceId = _conferenceId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateConferenceLocation`(
                _conferenceId  int(11)  
,_location  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Conference
                SET     location =  _location 

                WHERE
                        conferenceId = _conferenceId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateConferenceSummary`(
                _conferenceId  int(11)  
,_summary  text  

            )
BEGIN
                UPDATE  mAev_Conference
                SET     summary =  _summary 

                WHERE
                        conferenceId = _conferenceId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateConferenceTitle`(
                _conferenceId  int(11)  
,_title  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Conference
                SET     title =  _title 

                WHERE
                        conferenceId = _conferenceId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEvent`(
                            _eventId  int(11)  
,_title  varchar(50)  
,_category  enum('CultureWeek','FirstSemesterParty','GalaNight','Gaduation','GrillParty','Challenge','Mourning','Divers','Football','Tournament')  
,_date  datetime  
,_location  varchar(50)  
,_summary  text  

                  )
BEGIN
                UPDATE  mAev_Event
                SET     title =  _title 
,category =  _category 
,date =  _date 
,location =  _location 
,summary =  _summary 

                WHERE
                        eventId = _eventId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventBillSummary`(
                            _billSummaryId  int(11)  
,_eventId  int(11)  
,_title  varchar(50)  
,_summary  text  
,_summaryFileName  varchar(512)  

                  )
BEGIN
                UPDATE  mAev_EventBillSummary
                SET     eventId =  _eventId 
,title =  _title 
,summary =  _summary 
,summaryFileName =  _summaryFileName 

                WHERE
                        billSummaryId = _billSummaryId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventBillSummaryEventId`(
                _billSummaryId  int(11)  
,_eventId  int(11)  

            )
BEGIN
                UPDATE  mAev_EventBillSummary
                SET     eventId =  _eventId 

                WHERE
                        billSummaryId = _billSummaryId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventBillSummarySummary`(
                _billSummaryId  int(11)  
,_summary  text  

            )
BEGIN
                UPDATE  mAev_EventBillSummary
                SET     summary =  _summary 

                WHERE
                        billSummaryId = _billSummaryId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventBillSummarySummaryFileName`(
                _billSummaryId  int(11)  
,_summaryFileName  varchar(512)  

            )
BEGIN
                UPDATE  mAev_EventBillSummary
                SET     summaryFileName =  _summaryFileName 

                WHERE
                        billSummaryId = _billSummaryId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventBillSummaryTitle`(
                _billSummaryId  int(11)  
,_title  varchar(50)  

            )
BEGIN
                UPDATE  mAev_EventBillSummary
                SET     title =  _title 

                WHERE
                        billSummaryId = _billSummaryId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventCategory`(
                _eventId  int(11)  
,_category  enum('CultureWeek','FirstSemesterParty','GalaNight','Gaduation','GrillParty','Challenge','Mourning','Divers','Football','Tournament')  

            )
BEGIN
                UPDATE  mAev_Event
                SET     category =  _category 

                WHERE
                        eventId = _eventId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventDate`(
                _eventId  int(11)  
,_date  datetime  

            )
BEGIN
                UPDATE  mAev_Event
                SET     date =  _date 

                WHERE
                        eventId = _eventId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventLocation`(
                _eventId  int(11)  
,_location  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Event
                SET     location =  _location 

                WHERE
                        eventId = _eventId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventPhoto`(
                            _photoId  int(11)  
,_eventId  int(11)  
,_fileFullName  varchar(512)  
,_title  varchar(50)  

                  )
BEGIN
                UPDATE  mAev_EventPhoto
                SET     eventId =  _eventId 
,fileFullName =  _fileFullName 
,title =  _title 

                WHERE
                        photoId = _photoId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventPhotoComment`(
                            _photoCommentId  int(11)  
,_photoId  int(11)  
,_userId  int(11)  
,_commentText  text  
,_date  datetime  

                  )
BEGIN
                UPDATE  mAev_EventPhotoComment
                SET     photoId =  _photoId 
,userId =  _userId 
,commentText =  _commentText 
,date =  _date 

                WHERE
                        photoCommentId = _photoCommentId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventPhotoCommentCommentText`(
                _photoCommentId  int(11)  
,_commentText  text  

            )
BEGIN
                UPDATE  mAev_EventPhotoComment
                SET     commentText =  _commentText 

                WHERE
                        photoCommentId = _photoCommentId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventPhotoCommentDate`(
                _photoCommentId  int(11)  
,_date  datetime  

            )
BEGIN
                UPDATE  mAev_EventPhotoComment
                SET     date =  _date 

                WHERE
                        photoCommentId = _photoCommentId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventPhotoCommentPhotoId`(
                _photoCommentId  int(11)  
,_photoId  int(11)  

            )
BEGIN
                UPDATE  mAev_EventPhotoComment
                SET     photoId =  _photoId 

                WHERE
                        photoCommentId = _photoCommentId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventPhotoCommentUserId`(
                _photoCommentId  int(11)  
,_userId  int(11)  

            )
BEGIN
                UPDATE  mAev_EventPhotoComment
                SET     userId =  _userId 

                WHERE
                        photoCommentId = _photoCommentId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventPhotoEventId`(
                _photoId  int(11)  
,_eventId  int(11)  

            )
BEGIN
                UPDATE  mAev_EventPhoto
                SET     eventId =  _eventId 

                WHERE
                        photoId = _photoId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventPhotoFileFullName`(
                _photoId  int(11)  
,_fileFullName  varchar(512)  

            )
BEGIN
                UPDATE  mAev_EventPhoto
                SET     fileFullName =  _fileFullName 

                WHERE
                        photoId = _photoId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventPhotoTitle`(
                _photoId  int(11)  
,_title  varchar(50)  

            )
BEGIN
                UPDATE  mAev_EventPhoto
                SET     title =  _title 

                WHERE
                        photoId = _photoId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventSummary`(
                _eventId  int(11)  
,_summary  text  

            )
BEGIN
                UPDATE  mAev_Event
                SET     summary =  _summary 

                WHERE
                        eventId = _eventId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventTitle`(
                _eventId  int(11)  
,_title  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Event
                SET     title =  _title 

                WHERE
                        eventId = _eventId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventVideo`(
                            _videoId  int(11)  
,_eventId  int(11)  
,_fileFullName  varchar(512)  
,_title  varchar(50)  

                  )
BEGIN
                UPDATE  mAev_EventVideo
                SET     eventId =  _eventId 
,fileFullName =  _fileFullName 
,title =  _title 

                WHERE
                        videoId = _videoId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventVideoComment`(
                            _videoCommentId  int(11)  
,_videoId  int(11)  
,_userId  int(11)  
,_commentText  text  
,_date  datetime  

                  )
BEGIN
                UPDATE  mAev_EventVideoComment
                SET     videoId =  _videoId 
,userId =  _userId 
,commentText =  _commentText 
,date =  _date 

                WHERE
                        videoCommentId = _videoCommentId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventVideoCommentCommentText`(
                _videoCommentId  int(11)  
,_commentText  text  

            )
BEGIN
                UPDATE  mAev_EventVideoComment
                SET     commentText =  _commentText 

                WHERE
                        videoCommentId = _videoCommentId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventVideoCommentDate`(
                _videoCommentId  int(11)  
,_date  datetime  

            )
BEGIN
                UPDATE  mAev_EventVideoComment
                SET     date =  _date 

                WHERE
                        videoCommentId = _videoCommentId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventVideoCommentUserId`(
                _videoCommentId  int(11)  
,_userId  int(11)  

            )
BEGIN
                UPDATE  mAev_EventVideoComment
                SET     userId =  _userId 

                WHERE
                        videoCommentId = _videoCommentId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventVideoCommentVideoId`(
                _videoCommentId  int(11)  
,_videoId  int(11)  

            )
BEGIN
                UPDATE  mAev_EventVideoComment
                SET     videoId =  _videoId 

                WHERE
                        videoCommentId = _videoCommentId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventVideoEventId`(
                _videoId  int(11)  
,_eventId  int(11)  

            )
BEGIN
                UPDATE  mAev_EventVideo
                SET     eventId =  _eventId 

                WHERE
                        videoId = _videoId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventVideoFileFullName`(
                _videoId  int(11)  
,_fileFullName  varchar(512)  

            )
BEGIN
                UPDATE  mAev_EventVideo
                SET     fileFullName =  _fileFullName 

                WHERE
                        videoId = _videoId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateEventVideoTitle`(
                _videoId  int(11)  
,_title  varchar(50)  

            )
BEGIN
                UPDATE  mAev_EventVideo
                SET     title =  _title 

                WHERE
                        videoId = _videoId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateExpense`(
                            _expenseId  int(11)  
,_eventId  int(11)  
,_title  varchar(50)  
,_amount  double  
,_category  enum('CultureWeek','FirstSemesterParty','GalaNight','Gaduation','GrillParty','Challenge','Mourning','Donation','Sport','Divers')  
,_billFileName  varchar(512)  
,_transactionDate  datetime  

                  )
BEGIN
                UPDATE  mAev_Expense
                SET     eventId =  _eventId 
,title =  _title 
,amount =  _amount 
,category =  _category 
,billFileName =  _billFileName 
,transactionDate =  _transactionDate 

                WHERE
                        expenseId = _expenseId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateExpenseAmount`(
                _expenseId  int(11)  
,_amount  double  

            )
BEGIN
                UPDATE  mAev_Expense
                SET     amount =  _amount 

                WHERE
                        expenseId = _expenseId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateExpenseBillFileName`(
                _expenseId  int(11)  
,_billFileName  varchar(512)  

            )
BEGIN
                UPDATE  mAev_Expense
                SET     billFileName =  _billFileName 

                WHERE
                        expenseId = _expenseId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateExpenseCategory`(
                _expenseId  int(11)  
,_category  enum('CultureWeek','FirstSemesterParty','GalaNight','Gaduation','GrillParty','Challenge','Mourning','Donation','Sport','Divers')  

            )
BEGIN
                UPDATE  mAev_Expense
                SET     category =  _category 

                WHERE
                        expenseId = _expenseId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateExpenseEventId`(
                _expenseId  int(11)  
,_eventId  int(11)  

            )
BEGIN
                UPDATE  mAev_Expense
                SET     eventId =  _eventId 

                WHERE
                        expenseId = _expenseId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateExpenseTitle`(
                _expenseId  int(11)  
,_title  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Expense
                SET     title =  _title 

                WHERE
                        expenseId = _expenseId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateExpenseTransactionDate`(
                _expenseId  int(11)  
,_transactionDate  datetime  

            )
BEGIN
                UPDATE  mAev_Expense
                SET     transactionDate =  _transactionDate 

                WHERE
                        expenseId = _expenseId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateIncome`(
                            _incomeId  int(11)  
,_eventId  int(11)  
,_title  varchar(50)  
,_amount  double  
,_category  enum('CultureWeek','FirstSemesterParty','GalaNight','Gaduation','GrillParty','Challenge','Mourning','Donation','Sport','Divers')  
,_billFileName  varchar(512)  
,_transactionDate  datetime  

                  )
BEGIN
                UPDATE  mAev_Income
                SET     eventId =  _eventId 
,title =  _title 
,amount =  _amount 
,category =  _category 
,billFileName =  _billFileName 
,transactionDate =  _transactionDate 

                WHERE
                        incomeId = _incomeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateIncomeAmount`(
                _incomeId  int(11)  
,_amount  double  

            )
BEGIN
                UPDATE  mAev_Income
                SET     amount =  _amount 

                WHERE
                        incomeId = _incomeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateIncomeBillFileName`(
                _incomeId  int(11)  
,_billFileName  varchar(512)  

            )
BEGIN
                UPDATE  mAev_Income
                SET     billFileName =  _billFileName 

                WHERE
                        incomeId = _incomeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateIncomeCategory`(
                _incomeId  int(11)  
,_category  enum('CultureWeek','FirstSemesterParty','GalaNight','Gaduation','GrillParty','Challenge','Mourning','Donation','Sport','Divers')  

            )
BEGIN
                UPDATE  mAev_Income
                SET     category =  _category 

                WHERE
                        incomeId = _incomeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateIncomeDate`(
                _incomeId  int(11)  
,_date  datetime  

            )
BEGIN
                UPDATE  mAev_Income
                SET     date =  _date 

                WHERE
                        incomeId = _incomeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateIncomeEventId`(
                _incomeId  int(11)  
,_eventId  int(11)  

            )
BEGIN
                UPDATE  mAev_Income
                SET     eventId =  _eventId 

                WHERE
                        incomeId = _incomeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateIncomeTitle`(
                _incomeId  int(11)  
,_title  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Income
                SET     title =  _title 

                WHERE
                        incomeId = _incomeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateIncomeTransactionDate`(
                _incomeId  int(11)  
,_transactionDate  datetime  

            )
BEGIN
                UPDATE  mAev_Income
                SET     transactionDate =  _transactionDate 

                WHERE
                        incomeId = _incomeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateLogActivity`(
                            _activityId  int(11)  
,_userId  int(11)  
,_summary  text  
,_date  datetime  

                  )
BEGIN
                UPDATE  mAev_LogActivity
                SET     userId =  _userId 
,summary =  _summary 
,date =  _date 

                WHERE
                        activityId = _activityId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateLogActivityDate`(
                _activityId  int(11)  
,_date  datetime  

            )
BEGIN
                UPDATE  mAev_LogActivity
                SET     date =  _date 

                WHERE
                        activityId = _activityId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateLogActivitySummary`(
                _activityId  int(11)  
,_summary  text  

            )
BEGIN
                UPDATE  mAev_LogActivity
                SET     summary =  _summary 

                WHERE
                        activityId = _activityId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateLogActivityUserId`(
                _activityId  int(11)  
,_userId  int(11)  

            )
BEGIN
                UPDATE  mAev_LogActivity
                SET     userId =  _userId 

                WHERE
                        activityId = _activityId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMember`(
                            _memberId  int(11)  
,_gender  enum('Male','Female')  
,_name  varchar(50)  
,_lastName  varchar(50)  
,_email  varchar(100)  
,_phoneNumber  varchar(50)  
,_zipCode  varchar(10)  
,_city  varchar(50)  
,_address  varchar(256)  
,_position  varchar(50)  

                  )
BEGIN
                UPDATE  mAev_Member
                SET     gender =  _gender 
,name =  _name 
,lastName =  _lastName 
,email =  _email 
,phoneNumber =  _phoneNumber 
,zipCode =  _zipCode 
,city =  _city 
,address =  _address 
,position =  _position 

                WHERE
                        memberId = _memberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMemberAddress`(
                _memberId  int(11)  
,_address  varchar(256)  

            )
BEGIN
                UPDATE  mAev_Member
                SET     address =  _address 

                WHERE
                        memberId = _memberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMemberCity`(
                _memberId  int(11)  
,_city  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Member
                SET     city =  _city 

                WHERE
                        memberId = _memberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMemberEmail`(
                _memberId  int(11)  
,_email  varchar(100)  

            )
BEGIN
                UPDATE  mAev_Member
                SET     email =  _email 

                WHERE
                        memberId = _memberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMemberFee`(
                            _memberFeeId  int(11)  
,_memberId  int(11)  
,_amount  double  
,_billFileName  varchar(512)  
,_transactionDate  datetime  

                  )
BEGIN
                UPDATE  mAev_MemberFee
                SET     memberId =  _memberId 
,amount =  _amount 
,billFileName =  _billFileName 
,transactionDate =  _transactionDate 

                WHERE
                        memberFeeId = _memberFeeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMemberFeeAmount`(
                _memberFeeId  int(11)  
,_amount  double  

            )
BEGIN
                UPDATE  mAev_MemberFee
                SET     amount =  _amount 

                WHERE
                        memberFeeId = _memberFeeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMemberFeeBillFileName`(
                _memberFeeId  int(11)  
,_billFileName  varchar(512)  

            )
BEGIN
                UPDATE  mAev_MemberFee
                SET     billFileName =  _billFileName 

                WHERE
                        memberFeeId = _memberFeeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMemberFeeMemberId`(
                _memberFeeId  int(11)  
,_memberId  int(11)  

            )
BEGIN
                UPDATE  mAev_MemberFee
                SET     memberId =  _memberId 

                WHERE
                        memberFeeId = _memberFeeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMemberFeeTransactionDate`(
                _memberFeeId  int(11)  
,_transactionDate  datetime  

            )
BEGIN
                UPDATE  mAev_MemberFee
                SET     transactionDate =  _transactionDate 

                WHERE
                        memberFeeId = _memberFeeId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMemberGender`(
                _memberId  int(11)  
,_gender  enum('Male','Female')  

            )
BEGIN
                UPDATE  mAev_Member
                SET     gender =  _gender 

                WHERE
                        memberId = _memberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMemberLastName`(
                _memberId  int(11)  
,_lastName  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Member
                SET     lastName =  _lastName 

                WHERE
                        memberId = _memberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMemberName`(
                _memberId  int(11)  
,_name  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Member
                SET     name =  _name 

                WHERE
                        memberId = _memberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMemberPhoneNumber`(
                _memberId  int(11)  
,_phoneNumber  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Member
                SET     phoneNumber =  _phoneNumber 

                WHERE
                        memberId = _memberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMemberPosition`(
                _memberId  int(11)  
,_position  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Member
                SET     position =  _position 

                WHERE
                        memberId = _memberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateMemberZipCode`(
                _memberId  int(11)  
,_zipCode  varchar(10)  

            )
BEGIN
                UPDATE  mAev_Member
                SET     zipCode =  _zipCode 

                WHERE
                        memberId = _memberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateOfficeMember`(
                            _officeMemberId  int(11)  
,_memberId  int(11)  
,_position  enum('President','Secretary','ChiefCulture','ChiefSport','Treasurer')  

                  )
BEGIN
                UPDATE  mAev_OfficeMember
                SET     memberId =  _memberId 
,position =  _position 

                WHERE
                        officeMemberId = _officeMemberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateOfficeMemberMemberId`(
                _officeMemberId  int(11)  
,_memberId  int(11)  

            )
BEGIN
                UPDATE  mAev_OfficeMember
                SET     memberId =  _memberId 

                WHERE
                        officeMemberId = _officeMemberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateOfficeMemberPosition`(
                _officeMemberId  int(11)  
,_position  enum('President','Secretary','ChiefCulture','ChiefSport','Treasurer')  

            )
BEGIN
                UPDATE  mAev_OfficeMember
                SET     position =  _position 

                WHERE
                        officeMemberId = _officeMemberId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateOldExam`(
                            _examId  int(11)  
,_userId  int(11)  
,_subject  varchar(50)  
,_title  varchar(120)  
,_semester  varchar(10)  
,_date  datetime  
,_fileFullName  varchar(512)  

                  )
BEGIN
                UPDATE  mAev_OldExam
                SET     userId =  _userId 
,subject =  _subject 
,title =  _title 
,semester =  _semester 
,date =  _date 
,fileFullName =  _fileFullName 

                WHERE
                        examId = _examId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateOldExamDate`(
                _examId  int(11)  
,_date  datetime  

            )
BEGIN
                UPDATE  mAev_OldExam
                SET     date =  _date 

                WHERE
                        examId = _examId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateOldExamFileFullName`(
                _examId  int(11)  
,_fileFullName  varchar(512)  

            )
BEGIN
                UPDATE  mAev_OldExam
                SET     fileFullName =  _fileFullName 

                WHERE
                        examId = _examId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateOldExamSemester`(
                _examId  int(11)  
,_semester  varchar(10)  

            )
BEGIN
                UPDATE  mAev_OldExam
                SET     semester =  _semester 

                WHERE
                        examId = _examId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateOldExamSubject`(
                _examId  int(11)  
,_subject  varchar(50)  

            )
BEGIN
                UPDATE  mAev_OldExam
                SET     subject =  _subject 

                WHERE
                        examId = _examId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateOldExamTitle`(
                _examId  int(11)  
,_title  varchar(120)  

            )
BEGIN
                UPDATE  mAev_OldExam
                SET     title =  _title 

                WHERE
                        examId = _examId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateOldExamUserId`(
                _examId  int(11)  
,_userId  int(11)  

            )
BEGIN
                UPDATE  mAev_OldExam
                SET     userId =  _userId 

                WHERE
                        examId = _examId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateProject`(
                            _projectId  int(11)  
,_userId  int(11)  
,_title  varchar(50)  
,_summary  text  

                  )
BEGIN
                UPDATE  mAev_Project
                SET     userId =  _userId 
,title =  _title 
,summary =  _summary 

                WHERE
                        projectId = _projectId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateProjectSummary`(
                _projectId  int(11)  
,_summary  text  

            )
BEGIN
                UPDATE  mAev_Project
                SET     summary =  _summary 

                WHERE
                        projectId = _projectId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateProjectTitle`(
                _projectId  int(11)  
,_title  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Project
                SET     title =  _title 

                WHERE
                        projectId = _projectId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateProjectUserId`(
                _projectId  int(11)  
,_userId  int(11)  

            )
BEGIN
                UPDATE  mAev_Project
                SET     userId =  _userId 

                WHERE
                        projectId = _projectId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updatePublication`(
                            _publicationId  int(11)  
,_userId  int(11)  
,_category  enum('Info','Concert','Hiwi','Mourning')  
,_summary  text  
,_date  datetime  

                  )
BEGIN
                UPDATE  mAev_Publication
                SET     userId =  _userId 
,category =  _category 
,summary =  _summary 
,date =  _date 

                WHERE
                        publicationId = _publicationId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updatePublicationCategory`(
                _publicationId  int(11)  
,_category  enum('Info','Concert','Hiwi','Mourning')  

            )
BEGIN
                UPDATE  mAev_Publication
                SET     category =  _category 

                WHERE
                        publicationId = _publicationId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updatePublicationDate`(
                _publicationId  int(11)  
,_date  datetime  

            )
BEGIN
                UPDATE  mAev_Publication
                SET     date =  _date 

                WHERE
                        publicationId = _publicationId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updatePublicationSummary`(
                _publicationId  int(11)  
,_summary  text  

            )
BEGIN
                UPDATE  mAev_Publication
                SET     summary =  _summary 

                WHERE
                        publicationId = _publicationId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updatePublicationUserId`(
                _publicationId  int(11)  
,_userId  int(11)  

            )
BEGIN
                UPDATE  mAev_Publication
                SET     userId =  _userId 

                WHERE
                        publicationId = _publicationId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRole`(
                            _roleId  int(11)  
,_name  varchar(50)  
,_description  varchar(120)  

                  )
BEGIN
                UPDATE  mAev_Role
                SET     name =  _name 
,description =  _description 

                WHERE
                        roleId = _roleId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRoleAccessRight`(
                            _roleAccessId  int(11)  
,_roleId  int(11)  
,_entityType  varchar(50)  
,_createRight  bit(1)  
,_readRight  bit(1)  
,_editRight  bit(1)  
,_deleteRight  bit(1)  
,_fullRight  bit(1)  

                  )
BEGIN
                UPDATE  mAev_RoleAccessRight
                SET     roleId =  _roleId 
,entityType =  _entityType 
,createRight =  _createRight 
,readRight =  _readRight 
,editRight =  _editRight 
,deleteRight =  _deleteRight 
,fullRight =  _fullRight 

                WHERE
                        roleAccessId = _roleAccessId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRoleAccessRightCreateRight`(
                _roleAccessId  int(11)  
,_createRight  bit(1)  

            )
BEGIN
                UPDATE  mAev_RoleAccessRight
                SET     createRight =  _createRight 

                WHERE
                        roleAccessId = _roleAccessId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRoleAccessRightDeleteRight`(
                _roleAccessId  int(11)  
,_deleteRight  bit(1)  

            )
BEGIN
                UPDATE  mAev_RoleAccessRight
                SET     deleteRight =  _deleteRight 

                WHERE
                        roleAccessId = _roleAccessId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRoleAccessRightEditRight`(
                _roleAccessId  int(11)  
,_editRight  bit(1)  

            )
BEGIN
                UPDATE  mAev_RoleAccessRight
                SET     editRight =  _editRight 

                WHERE
                        roleAccessId = _roleAccessId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRoleAccessRightEntityType`(
                _roleAccessId  int(11)  
,_entityType  varchar(50)  

            )
BEGIN
                UPDATE  mAev_RoleAccessRight
                SET     entityType =  _entityType 

                WHERE
                        roleAccessId = _roleAccessId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRoleAccessRightFullRight`(
                _roleAccessId  int(11)  
,_fullRight  bit(1)  

            )
BEGIN
                UPDATE  mAev_RoleAccessRight
                SET     fullRight =  _fullRight 

                WHERE
                        roleAccessId = _roleAccessId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRoleAccessRightReadRight`(
                _roleAccessId  int(11)  
,_readRight  bit(1)  

            )
BEGIN
                UPDATE  mAev_RoleAccessRight
                SET     readRight =  _readRight 

                WHERE
                        roleAccessId = _roleAccessId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRoleAccessRightRoleId`(
                _roleAccessId  int(11)  
,_roleId  int(11)  

            )
BEGIN
                UPDATE  mAev_RoleAccessRight
                SET     roleId =  _roleId 

                WHERE
                        roleAccessId = _roleAccessId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRoleDescription`(
                _roleId  int(11)  
,_description  varchar(120)  

            )
BEGIN
                UPDATE  mAev_Role
                SET     description =  _description 

                WHERE
                        roleId = _roleId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRoleName`(
                _roleId  int(11)  
,_name  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Role
                SET     name =  _name 

                WHERE
                        roleId = _roleId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateTutorial`(
                            _tutorialId  int(11)  
,_memberId  int(11)  
,_date  datetime  
,_subject  varchar(50)  
,_level  varchar(50)  
,_location  varchar(50)  
,_shedules  varchar(50)  
,_status  enum('Cancelled','Active')  

                  )
BEGIN
                UPDATE  mAev_Tutorial
                SET     memberId =  _memberId 
,date =  _date 
,subject =  _subject 
,level =  _level 
,location =  _location 
,shedules =  _shedules 
,status =  _status 

                WHERE
                        tutorialId = _tutorialId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateTutorialDate`(
                _tutorialId  int(11)  
,_date  datetime  

            )
BEGIN
                UPDATE  mAev_Tutorial
                SET     date =  _date 

                WHERE
                        tutorialId = _tutorialId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateTutorialLevel`(
                _tutorialId  int(11)  
,_level  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Tutorial
                SET     level =  _level 

                WHERE
                        tutorialId = _tutorialId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateTutorialLocation`(
                _tutorialId  int(11)  
,_location  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Tutorial
                SET     location =  _location 

                WHERE
                        tutorialId = _tutorialId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateTutorialMemberId`(
                _tutorialId  int(11)  
,_memberId  int(11)  

            )
BEGIN
                UPDATE  mAev_Tutorial
                SET     memberId =  _memberId 

                WHERE
                        tutorialId = _tutorialId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateTutorialShedules`(
                _tutorialId  int(11)  
,_shedules  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Tutorial
                SET     shedules =  _shedules 

                WHERE
                        tutorialId = _tutorialId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateTutorialStatus`(
                _tutorialId  int(11)  
,_status  enum('Cancelled','Active')  

            )
BEGIN
                UPDATE  mAev_Tutorial
                SET     status =  _status 

                WHERE
                        tutorialId = _tutorialId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateTutorialSubject`(
                _tutorialId  int(11)  
,_subject  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Tutorial
                SET     subject =  _subject 

                WHERE
                        tutorialId = _tutorialId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUser`(
                            _userId  int(11)  
,_loginName  varchar(50)  
,_hashPassword  varchar(20)  
,_name  varchar(50)  
,_lastName  varchar(50)  
,_phoneNumber  varchar(50)  
,_email  varchar(100)  

                  )
BEGIN
                UPDATE  mAev_User
                SET     loginName =  _loginName 
,hashPassword =  _hashPassword 
,name =  _name 
,lastName =  _lastName 
,phoneNumber =  _phoneNumber 
,email =  _email 

                WHERE
                        userId = _userId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserEmail`(
                _userId  int(11)  
,_email  varchar(100)  

            )
BEGIN
                UPDATE  mAev_User
                SET     email =  _email 

                WHERE
                        userId = _userId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserHashPassword`(
                _userId  int(11)  
,_hashPassword  varchar(20)  

            )
BEGIN
                UPDATE  mAev_User
                SET     hashPassword =  _hashPassword 

                WHERE
                        userId = _userId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserLastName`(
                _userId  int(11)  
,_lastName  varchar(50)  

            )
BEGIN
                UPDATE  mAev_User
                SET     lastName =  _lastName 

                WHERE
                        userId = _userId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserLoginName`(
                _userId  int(11)  
,_loginName  varchar(50)  

            )
BEGIN
                UPDATE  mAev_User
                SET     loginName =  _loginName 

                WHERE
                        userId = _userId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserName`(
                _userId  int(11)  
,_name  varchar(50)  

            )
BEGIN
                UPDATE  mAev_User
                SET     name =  _name 

                WHERE
                        userId = _userId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserPhoneNumber`(
                _userId  int(11)  
,_phoneNumber  varchar(50)  

            )
BEGIN
                UPDATE  mAev_User
                SET     phoneNumber =  _phoneNumber 

                WHERE
                        userId = _userId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserProfile`(
                            _profileId  int(11)  
,_userId  int(11)  
,_gender  enum('Male','Female')  
,_photoFileName  varchar(256)  
,_street  varchar(50)  
,_zipCode  varchar(10)  
,_city  varchar(50)  
,_address  varchar(256)  
,_defalutLanguage  enum('DE','FR','EN')  

                  )
BEGIN
                UPDATE  mAev_UserProfile
                SET     userId =  _userId 
,gender =  _gender 
,photoFileName =  _photoFileName 
,street =  _street 
,zipCode =  _zipCode 
,city =  _city 
,address =  _address 
,defalutLanguage =  _defalutLanguage 

                WHERE
                        profileId = _profileId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserProfileAddress`(
                _profileId  int(11)  
,_address  varchar(256)  

            )
BEGIN
                UPDATE  mAev_UserProfile
                SET     address =  _address 

                WHERE
                        profileId = _profileId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserProfileCity`(
                _profileId  int(11)  
,_city  varchar(50)  

            )
BEGIN
                UPDATE  mAev_UserProfile
                SET     city =  _city 

                WHERE
                        profileId = _profileId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserProfileDefalutLanguage`(
                _profileId  int(11)  
,_defalutLanguage  enum('DE','FR','EN')  

            )
BEGIN
                UPDATE  mAev_UserProfile
                SET     defalutLanguage =  _defalutLanguage 

                WHERE
                        profileId = _profileId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserProfileGender`(
                _profileId  int(11)  
,_gender  enum('Male','Female')  

            )
BEGIN
                UPDATE  mAev_UserProfile
                SET     gender =  _gender 

                WHERE
                        profileId = _profileId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserProfilePhotoFileName`(
                _profileId  int(11)  
,_photoFileName  varchar(256)  

            )
BEGIN
                UPDATE  mAev_UserProfile
                SET     photoFileName =  _photoFileName 

                WHERE
                        profileId = _profileId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserProfileStreet`(
                _profileId  int(11)  
,_street  varchar(50)  

            )
BEGIN
                UPDATE  mAev_UserProfile
                SET     street =  _street 

                WHERE
                        profileId = _profileId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserProfileUserId`(
                _profileId  int(11)  
,_userId  int(11)  

            )
BEGIN
                UPDATE  mAev_UserProfile
                SET     userId =  _userId 

                WHERE
                        profileId = _profileId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUserProfileZipCode`(
                _profileId  int(11)  
,_zipCode  varchar(10)  

            )
BEGIN
                UPDATE  mAev_UserProfile
                SET     zipCode =  _zipCode 

                WHERE
                        profileId = _profileId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateVeteran`(
                            _veteranId  int(11)  
,_gender  enum('Male','Female')  
,_name  varchar(50)  
,_lastName  varchar(50)  
,_email  varchar(100)  
,_phoneNumber  varchar(50)  
,_zipCode  varchar(10)  
,_city  varchar(50)  
,_address  varchar(256)  
,_position  varchar(50)  

                  )
BEGIN
                UPDATE  mAev_Veteran
                SET     gender =  _gender 
,name =  _name 
,lastName =  _lastName 
,email =  _email 
,phoneNumber =  _phoneNumber 
,zipCode =  _zipCode 
,city =  _city 
,address =  _address 
,position =  _position 

                WHERE
                        veteranId = _veteranId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateVeteranAddress`(
                _veteranId  int(11)  
,_address  varchar(256)  

            )
BEGIN
                UPDATE  mAev_Veteran
                SET     address =  _address 

                WHERE
                        veteranId = _veteranId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateVeteranCity`(
                _veteranId  int(11)  
,_city  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Veteran
                SET     city =  _city 

                WHERE
                        veteranId = _veteranId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateVeteranEmail`(
                _veteranId  int(11)  
,_email  varchar(100)  

            )
BEGIN
                UPDATE  mAev_Veteran
                SET     email =  _email 

                WHERE
                        veteranId = _veteranId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateVeteranGender`(
                _veteranId  int(11)  
,_gender  enum('Male','Female')  

            )
BEGIN
                UPDATE  mAev_Veteran
                SET     gender =  _gender 

                WHERE
                        veteranId = _veteranId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateVeteranLastName`(
                _veteranId  int(11)  
,_lastName  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Veteran
                SET     lastName =  _lastName 

                WHERE
                        veteranId = _veteranId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateVeteranName`(
                _veteranId  int(11)  
,_name  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Veteran
                SET     name =  _name 

                WHERE
                        veteranId = _veteranId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateVeteranPhoneNumber`(
                _veteranId  int(11)  
,_phoneNumber  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Veteran
                SET     phoneNumber =  _phoneNumber 

                WHERE
                        veteranId = _veteranId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateVeteranPosition`(
                _veteranId  int(11)  
,_position  varchar(50)  

            )
BEGIN
                UPDATE  mAev_Veteran
                SET     position =  _position 

                WHERE
                        veteranId = _veteranId;
            END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateVeteranZipCode`(
                _veteranId  int(11)  
,_zipCode  varchar(10)  

            )
BEGIN
                UPDATE  mAev_Veteran
                SET     zipCode =  _zipCode 

                WHERE
                        veteranId = _veteranId;
            END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mAev_Conference`
--

CREATE TABLE IF NOT EXISTS `mAev_Conference` (
  `conferenceId` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `summary` text,
  PRIMARY KEY (`conferenceId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `mAev_Conference`
--

INSERT INTO `mAev_Conference` VALUES
(12, '2017-04-12 22:00:00', 'title-3798003', 'location-370788c', 'sjsjs'),
(13, '2017-04-30 00:00:00', 'Diaspora camerounaise', 'Heilbronn', 'Dirigee par Valere.\r\nAlways have handy the un-minified CSS for bootstrap so you can see what styles they have on their components, then create a CSS file AFTER it, if you don''t use LESS and over-write their mixins or whatever\r\n\r\nThis is the default modal css for 768px and up:\r\n\r\n@media (min-width: 768px) {\r\n  .modal-dialog {\r\n    width: 600px;\r\n    margin: 30px auto;\r\n  }\r\n  ...\r\n}\r\n\r\nThey have a class modal-lg for larger widths\r\n\r\n@media (min-width: 992px) {\r\n  .modal-lg {\r\n    width: 900px;\r\n  }\r\n}');

-- --------------------------------------------------------

--
-- Table structure for table `mAev_Event`
--

CREATE TABLE IF NOT EXISTS `mAev_Event` (
  `eventId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `category` enum('CultureWeek','FirstSemesterParty','GalaNight','Gaduation','GrillParty','Challenge','Mourning','Divers','Football','Tournament') NOT NULL DEFAULT 'Divers',
  `date` datetime NOT NULL,
  `location` varchar(50) NOT NULL,
  `summary` text,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `mAev_Event`
--

INSERT INTO `mAev_Event` VALUES
(21, 'Tournoi foot 2', 'Tournament', '2017-04-08 22:00:00', '-s', '<html>\n    <head>\n        <script type="text/javascript">\n            function alertFilename()\n            {\n                var thefile = document.getElementById(''thefile'');\n                alert(thefile.value);\n            }\n        </script>\n    </head>\n    <body>\n        <form>\n            <input type="file" id="thefile" onchange="alertFilename()" />\n            <input type="button"  #shsonclick="alertFilename()" value=''sww'' />\n        </form>\n    </body>\n</html>\n\n\n\nDuring the development of the .NET Framework, the class libraries were originally written using a managed code compiler system called Simple Managed C (SMC).[14][15] In January 1999, Anders Hejlsberg formed a team to build a new language at the time called Cool, which stood for "C-like Object Oriented Language".[16] Microsoft had considered keeping the name "Cool" as the final name of the language, but chose not to do so for trademark reasons. By the time the .NET project was publicly announced at the July 2000 Professional Developers Conference, the language had been renamed C#, and the class libraries and ASP.NET runtime had been ported to C#.\n\nC#''s principal designer and lead architect at Microsoft is Anders Hejlsberg, who was previously involved with the design of Turbo Pascal, Embarcadero Delphi (formerly CodeGear Delphi, Inprise Delphi and Borland Delphi), and Visual J++. In interviews and technical papers he has stated that flaws[citation needed] in most major programming languages (e.g. C++, Java, Delphi, and Smalltalk) drove the fundamentals of the Common Language Runtime (CLR), which, in turn, drove the design of the C# language itself.\n\nJames Gosling, who created the Java programming language in 1994, and Bill Joy, a co-founder of Sun Microsystems, the originator of Java, called C# an "imitation" of Java; Gosling further said that "[C# is] sort of Java with reliability, productivity and security deleted."[17][18] Klaus Kreft and Angelika Langer (authors of a C++ streams book) stated in a blog post that "Java and C# are almost identical programming languages. Boring repetition that lacks innovation,"[19] "Hardly anybody will claim that Java or C# are revolutionary programming languages that changed the way we write programs," and "C# borrowed a lot from Java - and vice versa. Now that C# supports boxing and unboxing, we''ll have a very similar feature in Java.\n"[20] In July 2000, Anders Hejlsberg said that C# is "not a Java clone" and is "much closer to C++" in its design.[21]'),
(22, 'Gala KA', 'GalaNight', '2017-04-30 22:00:00', 'Heilbronn', 'aaaaaa');

-- --------------------------------------------------------

--
-- Table structure for table `mAev_EventBillSummary`
--

CREATE TABLE IF NOT EXISTS `mAev_EventBillSummary` (
  `billSummaryId` int(11) NOT NULL AUTO_INCREMENT,
  `eventId` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `summary` text NOT NULL,
  `summaryFileName` varchar(512) NOT NULL,
  PRIMARY KEY (`billSummaryId`),
  KEY `eventId` (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mAev_EventPhoto`
--

CREATE TABLE IF NOT EXISTS `mAev_EventPhoto` (
  `photoId` int(11) NOT NULL AUTO_INCREMENT,
  `eventId` int(11) NOT NULL,
  `fileFullName` varchar(512) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`photoId`),
  KEY `eventId` (`eventId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `mAev_EventPhoto`
--

INSERT INTO `mAev_EventPhoto` VALUES
(5, 21, 'p9.jpeg', '-tees'),
(6, 21, 'p10.jpeg', '-tees'),
(7, 21, 'p11.jpeg', '-tees'),
(8, 21, 'p12.jpeg', '-tees'),
(9, 21, 'p13.jpeg', '-tees'),
(10, 21, 'p14.jpeg', '-tees');

-- --------------------------------------------------------

--
-- Table structure for table `mAev_EventPhotoComment`
--

CREATE TABLE IF NOT EXISTS `mAev_EventPhotoComment` (
  `photoCommentId` int(11) NOT NULL AUTO_INCREMENT,
  `photoId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `commentText` text,
  `date` datetime NOT NULL,
  PRIMARY KEY (`photoCommentId`),
  KEY `photoId` (`photoId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mAev_EventVideo`
--

CREATE TABLE IF NOT EXISTS `mAev_EventVideo` (
  `videoId` int(11) NOT NULL AUTO_INCREMENT,
  `eventId` int(11) NOT NULL,
  `fileFullName` varchar(512) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`videoId`),
  KEY `eventId` (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mAev_EventVideoComment`
--

CREATE TABLE IF NOT EXISTS `mAev_EventVideoComment` (
  `videoCommentId` int(11) NOT NULL AUTO_INCREMENT,
  `videoId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `commentText` text,
  `date` datetime NOT NULL,
  PRIMARY KEY (`videoCommentId`),
  KEY `videoId` (`videoId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mAev_Expense`
--

CREATE TABLE IF NOT EXISTS `mAev_Expense` (
  `expenseId` int(11) NOT NULL AUTO_INCREMENT,
  `eventId` int(11) DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `category` enum('CultureWeek','FirstSemesterParty','GalaNight','Gaduation','GrillParty','Challenge','Mourning','Donation','Sport','Divers') NOT NULL DEFAULT 'Divers',
  `billFileName` varchar(512) DEFAULT NULL,
  `transactionDate` datetime NOT NULL,
  PRIMARY KEY (`expenseId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `mAev_Expense`
--

INSERT INTO `mAev_Expense` VALUES
(35, 19, 'Match Amical contre Pforzheim', 110, 'Sport', 'img_4654.jpg', '2016-08-24 22:00:00'),
(36, 18, 'Amende', 75, 'Challenge', 'img_4650.jpg', '2016-11-09 23:00:00'),
(37, 18, 'Frais de membres Challenge', 80, 'Challenge', 'img_4650.jpg', '2016-05-19 22:00:00'),
(38, 0, 'Fais du Gymnase Handball', 120, 'Sport', 'img_4649.jpg', '2016-05-19 22:00:00'),
(39, 18, 'Afiliation Challenge Camerounais', 400, 'Challenge', 'img_4648.jpg', '2016-11-19 23:00:00'),
(40, 16, 'Tournois Football Germersheim', 50, 'Sport', 'img_4647.jpg', '2017-05-13 22:00:00'),
(41, 18, 'Maillots de Equipe', 422.3, 'Challenge', 'img_4653.jpg', '2017-05-20 15:21:39'),
(42, 17, 'Tournois Football Karlsruhe', 50, 'Sport', 'img_4647.jpg', '2017-05-03 22:00:00'),
(43, 0, 'Sport Insfraktutur - Football', 135, 'Sport', 'img_4652.jpg', '2017-05-12 22:00:00'),
(44, 20, 'Tournois Mannheim', 50, 'Sport', 'img_4647.jpg', '2017-05-11 22:00:00'),
(45, 0, 'Wesbite Hosting - kameruner-heilbronn.de', 40, 'Divers', 'facture-site-kameruner-heilbronn.de.pdf', '2017-05-20 15:38:51'),
(46, 22, 'dssd', 11, 'Challenge', 'img_0024.jpg', '2017-07-10 07:43:24'),
(47, 22, '-sss', 111, 'CultureWeek', 'fraisEquipements.png', '2017-07-11 07:41:04');

-- --------------------------------------------------------

--
-- Table structure for table `mAev_Income`
--

CREATE TABLE IF NOT EXISTS `mAev_Income` (
  `incomeId` int(11) NOT NULL AUTO_INCREMENT,
  `eventId` int(11) DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `category` enum('CultureWeek','FirstSemesterParty','GalaNight','Gaduation','GrillParty','Challenge','Mourning','Donation','Sport','Divers') NOT NULL DEFAULT 'Divers',
  `billFileName` varchar(512) DEFAULT NULL,
  `transactionDate` datetime NOT NULL,
  PRIMARY KEY (`incomeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `mAev_Income`
--

INSERT INTO `mAev_Income` VALUES
(4, 0, 'Frais de Membres', 750, 'Divers', '', '2017-03-19 23:00:00'),
(5, 0, 'Maillots & Survetements', 540, 'Challenge', 'img_4653.jpg', '2017-05-20 15:41:52'),
(6, 19, 'Reste match pforzheim', 37.27, 'Sport', 'img_4654.jpg', '2016-08-26 22:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `mAev_LogActivity`
--

CREATE TABLE IF NOT EXISTS `mAev_LogActivity` (
  `activityId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `summary` text,
  `date` datetime NOT NULL,
  PRIMARY KEY (`activityId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mAev_Member`
--

CREATE TABLE IF NOT EXISTS `mAev_Member` (
  `memberId` int(11) NOT NULL AUTO_INCREMENT,
  `gender` enum('Male','Female') NOT NULL DEFAULT 'Male',
  `name` varchar(50) NOT NULL,
  `lastName` varchar(50) DEFAULT '',
  `email` varchar(100) DEFAULT NULL,
  `phoneNumber` varchar(50) NOT NULL,
  `zipCode` varchar(10) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`memberId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `mAev_Member`
--

INSERT INTO `mAev_Member` VALUES
(9, 'Male', 'Akouen', ' Prudence', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(13, 'Male', 'Bami Watcho', 'Guy', 'guybami@yahoo.fr', '017627288302', '74172', 'Neckarsulm', 'Sebastian-Bach-Weg 16', 'Président'),
(14, 'Female', 'Bokem Atchomou', 'Rodrigue', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(15, 'Male', 'Bomba', 'Rodrigue', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(16, 'Male', 'Damfe Dandjeu', 'Cedric Arnaud', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(17, 'Male', 'Dikoka Ngando', 'Dorette', '-', '-', '-', 'Heilbronn', '-', 'Chargé Activités Culturelles'),
(18, 'Male', 'Donmeza Penka', 'Paul vermon', '-', '017624360663', '-', 'Heilbronn', '-', 'Secretaire'),
(19, 'Male', 'Yamdjeu', 'Duplex', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(20, 'Male', 'Egbekaseh Assick', 'Robinson', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(21, 'Male', 'Foaleng', 'Denis', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(22, 'Male', 'Fotso Ouafo', 'Raoul', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(23, 'Male', 'Galiatcha Siekapen', 'Max Achille', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(24, 'Male', 'Kapawe', 'Levis', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(25, 'Male', 'Kaze Kenne', 'Valere', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(26, 'Female', 'Youtamba', 'Kevine', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(27, 'Male', 'Magni', 'Lucas Edgard', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(28, 'Male', 'Manock Bayap', 'Thierry', '-', '-', '-', 'Heilbronn', '-', 'Chargé Activités Sportivités'),
(29, 'Male', 'Misse Misse', 'Achille', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(30, 'Male', 'Ngague Toualeu', 'Serge Amede', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(31, 'Male', 'Ngoka', 'Wilfried', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(32, 'Male', 'Nkongo Essobo', 'Pecos', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(33, 'Male', 'Ntouala Kameni', 'Jean Desire', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(34, 'Male', 'Nya Djanwa', 'Reuel Patrick', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(35, 'Male', 'Nyetindema Fomete', 'Franck Gill', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(36, 'Male', 'Tiatcho', 'Patrick', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(37, 'Male', 'Talla Ouafeu', 'Edmond', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(38, 'Male', 'Tamdem Wabo', 'Aristide Brice', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(39, 'Male', 'Youtamba Fotso', 'Patrick', '-', '-', '-', 'Heilbronn', '-', 'Membre'),
(40, 'Female', 'Keudjio Dipah', 'Christelle Larissa', '-', '-', '-', 'Heilbronn', '-', 'Tresoriere');

-- --------------------------------------------------------

--
-- Table structure for table `mAev_MemberFee`
--

CREATE TABLE IF NOT EXISTS `mAev_MemberFee` (
  `memberFeeId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `billFileName` varchar(512) DEFAULT NULL,
  `transactionDate` datetime NOT NULL,
  PRIMARY KEY (`memberFeeId`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mAev_OfficeMember`
--

CREATE TABLE IF NOT EXISTS `mAev_OfficeMember` (
  `officeMemberId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `position` enum('President','Secretary','ChiefCulture','ChiefSport','Treasurer') NOT NULL,
  PRIMARY KEY (`officeMemberId`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `mAev_OfficeMember`
--

INSERT INTO `mAev_OfficeMember` VALUES
(1, 13, 'President'),
(2, 17, 'ChiefCulture'),
(3, 18, 'Secretary'),
(4, 28, 'ChiefSport'),
(5, 40, 'Treasurer');

-- --------------------------------------------------------

--
-- Table structure for table `mAev_OldExam`
--

CREATE TABLE IF NOT EXISTS `mAev_OldExam` (
  `examId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `title` varchar(120) NOT NULL,
  `semester` varchar(10) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `fileFullName` varchar(512) NOT NULL,
  PRIMARY KEY (`examId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `mAev_OldExam`
--

INSERT INTO `mAev_OldExam` VALUES
(5, 5, 'subject-saa', 'title-887433a', 'saad', '2017-07-11 12:07:10', 'facturePerdue06.06.17.png'),
(6, 5, 'Technische Mechanik II', 'Technische Mechanik II', 'Sommerseme', '2017-07-11 15:38:48', 'tm2_ss12.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `mAev_Project`
--

CREATE TABLE IF NOT EXISTS `mAev_Project` (
  `projectId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `summary` text,
  PRIMARY KEY (`projectId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mAev_Publication`
--

CREATE TABLE IF NOT EXISTS `mAev_Publication` (
  `publicationId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `category` enum('Info','Concert','Hiwi','Mourning') NOT NULL,
  `summary` text,
  `date` datetime NOT NULL,
  PRIMARY KEY (`publicationId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mAev_Role`
--

CREATE TABLE IF NOT EXISTS `mAev_Role` (
  `roleId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mAev_RoleAccessRight`
--

CREATE TABLE IF NOT EXISTS `mAev_RoleAccessRight` (
  `roleAccessId` int(11) NOT NULL AUTO_INCREMENT,
  `roleId` int(11) NOT NULL,
  `entityType` varchar(50) NOT NULL,
  `createRight` bit(1) NOT NULL DEFAULT b'0',
  `readRight` bit(1) NOT NULL DEFAULT b'0',
  `editRight` bit(1) NOT NULL DEFAULT b'0',
  `deleteRight` bit(1) NOT NULL DEFAULT b'0',
  `fullRight` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`roleAccessId`),
  KEY `roleId` (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mAev_Tutorial`
--

CREATE TABLE IF NOT EXISTS `mAev_Tutorial` (
  `tutorialId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `subject` varchar(50) NOT NULL,
  `level` varchar(50) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `shedules` varchar(50) DEFAULT NULL,
  `status` enum('Cancelled','Active') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`tutorialId`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mAev_User`
--

CREATE TABLE IF NOT EXISTS `mAev_User` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `loginName` varchar(50) NOT NULL,
  `hashPassword` varchar(20) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `lastName` varchar(50) DEFAULT '',
  `phoneNumber` varchar(50) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `loginName` (`loginName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `mAev_User`
--

INSERT INTO `mAev_User` VALUES
(1, 'testuser', '1234', 'Atango', 'Emile', '789996', 'email-bcbf5e0'),
(2, 'root', '11', 'abcd', 'shshs', '23323', 'guybami@xml.de'),
(3, 'loginName-e46bc06', '1111', 'name-8137471', 'lastName-35c5a2c', 'phoneNumber-4dff7cc', 'email-bcbf5e0'),
(4, 'root11', '111', 'dddd', 'Bami Watcho', '071327049016', 'oliver.somo@yahoo.fr'),
(5, 'guybami', '1234', 'dddd', 'fddf', '1111', 'dfdf');

-- --------------------------------------------------------

--
-- Table structure for table `mAev_UserProfile`
--

CREATE TABLE IF NOT EXISTS `mAev_UserProfile` (
  `profileId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `gender` enum('Male','Female') NOT NULL DEFAULT 'Male',
  `photoFileName` varchar(256) DEFAULT NULL,
  `street` varchar(50) DEFAULT NULL,
  `zipCode` varchar(10) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `defalutLanguage` enum('DE','FR','EN') NOT NULL DEFAULT 'FR',
  PRIMARY KEY (`profileId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mAev_Veteran`
--

CREATE TABLE IF NOT EXISTS `mAev_Veteran` (
  `veteranId` int(11) NOT NULL AUTO_INCREMENT,
  `gender` enum('Male','Female') NOT NULL DEFAULT 'Male',
  `name` varchar(50) NOT NULL,
  `lastName` varchar(50) DEFAULT '',
  `email` varchar(100) DEFAULT NULL,
  `phoneNumber` varchar(50) NOT NULL,
  `zipCode` varchar(10) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`veteranId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mAev_EventBillSummary`
--
ALTER TABLE `mAev_EventBillSummary`
  ADD CONSTRAINT `mAev_eventbillsummary_ibfk_1` FOREIGN KEY (`eventId`) REFERENCES `mAev_Event` (`eventId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mAev_EventPhoto`
--
ALTER TABLE `mAev_EventPhoto`
  ADD CONSTRAINT `mAev_eventphoto_ibfk_1` FOREIGN KEY (`eventId`) REFERENCES `mAev_Event` (`eventId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mAev_EventPhotoComment`
--
ALTER TABLE `mAev_EventPhotoComment`
  ADD CONSTRAINT `mAev_eventphotocomment_ibfk_1` FOREIGN KEY (`photoId`) REFERENCES `mAev_EventPhoto` (`photoId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mAev_eventphotocomment_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `mAev_User` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mAev_EventVideo`
--
ALTER TABLE `mAev_EventVideo`
  ADD CONSTRAINT `mAev_eventvideo_ibfk_1` FOREIGN KEY (`eventId`) REFERENCES `mAev_Event` (`eventId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mAev_EventVideoComment`
--
ALTER TABLE `mAev_EventVideoComment`
  ADD CONSTRAINT `mAev_eventvideocomment_ibfk_1` FOREIGN KEY (`videoId`) REFERENCES `mAev_EventVideo` (`videoId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mAev_eventvideocomment_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `mAev_User` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mAev_LogActivity`
--
ALTER TABLE `mAev_LogActivity`
  ADD CONSTRAINT `mAev_logactivity_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `mAev_User` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mAev_MemberFee`
--
ALTER TABLE `mAev_MemberFee`
  ADD CONSTRAINT `mAev_memberfee_ibfk_1` FOREIGN KEY (`memberId`) REFERENCES `mAev_Member` (`memberId`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `mAev_OfficeMember`
--
ALTER TABLE `mAev_OfficeMember`
  ADD CONSTRAINT `mAev_officemember_ibfk_1` FOREIGN KEY (`memberId`) REFERENCES `mAev_Member` (`memberId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mAev_OldExam`
--
ALTER TABLE `mAev_OldExam`
  ADD CONSTRAINT `mAev_oldexam_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `mAev_User` (`userId`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `mAev_Project`
--
ALTER TABLE `mAev_Project`
  ADD CONSTRAINT `mAev_project_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `mAev_User` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mAev_Publication`
--
ALTER TABLE `mAev_Publication`
  ADD CONSTRAINT `mAev_publication_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `mAev_User` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mAev_RoleAccessRight`
--
ALTER TABLE `mAev_RoleAccessRight`
  ADD CONSTRAINT `mAev_roleaccessright_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `mAev_Role` (`roleId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mAev_Tutorial`
--
ALTER TABLE `mAev_Tutorial`
  ADD CONSTRAINT `mAev_tutorial_ibfk_1` FOREIGN KEY (`memberId`) REFERENCES `mAev_Member` (`memberId`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `mAev_UserProfile`
--
ALTER TABLE `mAev_UserProfile`
  ADD CONSTRAINT `mAev_userprofile_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `mAev_User` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;
 
