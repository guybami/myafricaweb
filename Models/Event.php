
<?php

/**
 * This module was auto generated by the G-Watcho module generator
 * The Event model entity class.
 * @author
 *    Guy Bami
 */
include_once '../Utils/ExceptionLogger.php';

class Event {

    private $eventId;
    private $title;
    private $category;
    private $date;
    private $location;
    private $summary;

    /** Constructor of an Event object
     *  @param $eventId int The entity  primary key field 
     * @param  $title string The entity  title field 
     * @param  $category string The entity  category field 
     * @param  $date string The entity  date field 
     * @param  $location string The entity  location field 
     * @param  $summary string The entity  summary field 
     */
    function __construct($eventId = "", $title = "", $category = "", $date = "", $location = "", $summary = "") {
        $this->eventId = $eventId;
        $this->title = $title;
        $this->category = $category;
        $this->date = $date;
        $this->location = $location;
        $this->summary = $summary;
    }

    /**
     * Gets  $title value
     * @param $title
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Gets  $category value
     * @param $category
     * @return mixed
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * Gets  $date value
     * @param $date
     * @return mixed
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Gets  $location value
     * @param $location
     * @return mixed
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * Gets  $summary value
     * @param $summary
     * @return mixed
     */
    public function getSummary() {
        return $this->summary;
    }

    /**
     * Sets  $title value
     * @param $title
     * @return void
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * Sets  $category value
     * @param $category
     * @return void
     */
    public function setCategory($category) {
        $this->category = $category;
    }

    /**
     * Sets  $date value
     * @param $date
     * @return void
     */
    public function setDate($date) {
        $this->date = $date;
    }

    /**
     * Sets  $location value
     * @param $location
     * @return void
     */
    public function setLocation($location) {
        $this->location = $location;
    }

    /**
     * Sets  $summary value
     * @param $summary
     * @return void
     */
    public function setSummary($summary) {
        $this->summary = $summary;
    }

    /**
     * Selects all Event items
     * @return
     *   array The object having all Event items
     *    or string with the Exception details if error occured
     */
    public function selectAllEvents() {
        try {
            $daoSelect = new DaoSelect();
            return $daoSelect->selectAllEvents();
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Gets Event item
     * @param $eventId int  The table primary key
     * @return
     *   array The object with the given $eventId value
     */
    public function getEventDetails($eventId) {
        try {
            $daoSelect = new DaoSelect();
            return $daoSelect->selectEventDetails($eventId);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Inserts new Event entity
     * @param  $title string The entity  title field 
     * @param   $category string The entity  category field
     * @param   $date string The entity  date field
     * @param   $location string The entity  location field
     * @param   $summary string The entity  summary field
     * @return
     *   boolean TRUE if insert successful, otherwise FALSE
     */
    public function insertNewEvent($title, $category, $date, $location, $summary) {
        try {
            $daoInsert = new DaoInsert();
            return $daoInsert->insertNewEvent($title, $category, $date, $location, $summary);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Updates Event item
     * @param  $eventId int The entity  eventId field 
     * @param  $title string The entity  title field 
     * @param  $category string The entity  category field 
     * @param  $date string The entity  date field 
     * @param  $location string The entity  location field 
     * @param  $summary string The entity  summary field 
     * @return
     *   boolean  TRUE if update successful, otherwise FALSE
     */
    public function updateEvent($eventId, $title, $category, $date, $location, $summary) {
        try {
            $daoUpdate = new DaoUpdate();
            return $daoUpdate->updateEvent($eventId, $title, $category, $date, $location, $summary);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Updates Event item  details

     * @param  $eventId int The entity  eventId field 
     * @param  $title string The entity  title field 
     * @param  $category string The entity  category field 
     * @param  $date string The entity  date field 
     * @param  $location string The entity  location field 
     * @param  $summary string The entity  summary field 
     * @return
     *   boolean TRUE if update successful, otherwise FALSE
     */
    public function updateEventDetails($eventId, $title, $category, $date, $location, $summary) {
        try {
            $daoUpdate = new DaoUpdate();
            return $daoUpdate->updateEventDetails($eventId, $title, $category, $date, $location, $summary);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Updates title of the Event entity
     * @param   $eventId int The entity  eventId field 
     * @param   $title string The entity  title field
     * @return
     *   boolean TRUE if update successful, otherwise FALSE
     */
    public function updateEventTitle($eventId, $title) {
        try {
            $daoUpdate = new DaoUpdate();
            return $daoUpdate->updateEventTitle($eventId, $title);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Updates category of the Event entity
     * @param   $eventId int The entity  eventId field 
     * @param   $category string The entity  category field
     * @return
     *   boolean TRUE if update successful, otherwise FALSE
     */
    public function updateEventCategory($eventId, $category) {
        try {
            $daoUpdate = new DaoUpdate();
            return $daoUpdate->updateEventCategory($eventId, $category);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Updates date of the Event entity
     * @param   $eventId int The entity  eventId field 
     * @param   $date string The entity  date field
     * @return
     *   boolean TRUE if update successful, otherwise FALSE
     */
    public function updateEventDate($eventId, $date) {
        try {
            $daoUpdate = new DaoUpdate();
            return $daoUpdate->updateEventDate($eventId, $date);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Updates location of the Event entity
     * @param   $eventId int The entity  eventId field 
     * @param   $location string The entity  location field
     * @return
     *   boolean TRUE if update successful, otherwise FALSE
     */
    public function updateEventLocation($eventId, $location) {
        try {
            $daoUpdate = new DaoUpdate();
            return $daoUpdate->updateEventLocation($eventId, $location);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Updates summary of the Event entity
     * @param   $eventId int The entity  eventId field 
     * @param   $summary string The entity  summary field
     * @return
     *   boolean TRUE if update successful, otherwise FALSE
     */
    public function updateEventSummary($eventId, $summary) {
        try {
            $daoUpdate = new DaoUpdate();
            return $daoUpdate->updateEventSummary($eventId, $summary);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Updates specific field of the Event item
     * @param    $fieldName string The field name
     * @param    $keyFieldValue int The primary key field value
     * @param    $newFieldValue string The new field value
     * @return
     *   boolean TRUE if update successful, otherwise FALSE
     */
    public function updateEventEntityField($fieldName, $keyFieldValue, $newFieldValue) {
        try {
            switch ($fieldName) {

                case "title":
                    return $this->updateEventTitle($keyFieldValue, $newFieldValue);

                case "category":
                    return $this->updateEventCategory($keyFieldValue, $newFieldValue);

                case "date":
                    return $this->updateEventDate($keyFieldValue, $newFieldValue);

                case "location":
                    return $this->updateEventLocation($keyFieldValue, $newFieldValue);

                case "summary":
                    return $this->updateEventSummary($keyFieldValue, $newFieldValue);
            }
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
        return true;
    }

    /**
     * Deletes Event item
     * @param  $eventId int  The table primary key
     * @return    boolean|mixed TRUE if delete successful, otherwise string with error message
     */
    public function deleteEvent($eventId) {
        try {
            $daoDelete = new DaoDelete();
            return $daoDelete->deleteEvent($eventId);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Deletes selected Event items
     * @param  $selectedItemsId array The List of primary keys item to be deleted
     * @return boolean|mixed TRUE if delete successful, otherwise string with error message
     */
    public function deleteSelectedEvents($selectedItemsId) {

        try {
            $daoDelete = new DaoDelete();
            if (!isset($selectedItemsId) || !is_array($selectedItemsId)) {
                return "Error: Invalid Parameters type for this method.";
            }
            foreach (array_values($selectedItemsId) as $itemId) {
                if ($daoDelete->deleteEvent($itemId)) {
                    continue;
                } else {
                    return "error: Can not delete Event !";
                }
            }
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
        return true;
    }

    /**
     * Deletes all Event items
     * @return    boolean|mixed TRUE if delete successful, otherwise string with error message
     */
    public function deleteAllEvents() {
        try {
            $daoDelete = new DaoDelete();
            return $daoDelete->deleteAllEvents();
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }
    
    
    /*Custom methods*/
    /**
     * Selects all Event items by category
     * @return
     *   array The object having  Event items of the category
     *    or string with the Exception details if error occured
     */
    public function getAllEventsByCategory($category) {
        try {
            $daoSelect = new DaoSelect();
            return $daoSelect->getAllEventsByCategory($category);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    
    
}
