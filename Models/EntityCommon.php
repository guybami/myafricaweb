<?php

/**
 * EntityCommon class contains all data model entities
 *
 * @version 1.0
 * @author gbami
 */
include_once "BaseEntity.php";
include_once "Conference.php";
include_once "Event.php";
include_once "EventBillSummary.php";
include_once "EventPhoto.php";
include_once "EventPhotoComment.php";
include_once "EventVideo.php";
include_once "EventVideoComment.php";
include_once "Expense.php";
include_once "Income.php";
include_once "LogActivity.php";
include_once "Member.php";
include_once "MemberFee.php";
include_once "OfficeMember.php";
include_once "OldExam.php";
include_once "Project.php";
include_once "Publication.php";
include_once "Role.php";
include_once "RoleAccessRight.php";
include_once "User.php";
include_once "UserProfile.php";
include_once "Veteran.php";




abstract class EntityCommon
{
}