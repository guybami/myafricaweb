
<?php

/**
 * This module was auto generated by the G-Watcho module generator
 * The Role model entity class.
 * @author
 *    Guy Bami
 */
include_once '../Utils/ExceptionLogger.php';

class Role {

    private $roleId;
    private $name;
    private $description;

    /** Constructor of an Role object
     *  @param $roleId int The entity  primary key field 
     * @param  $name string The entity  name field 
     * @param  $description string The entity  description field 
     */
    function __construct($roleId = "", $name = "", $description = "") {
        $this->roleId = $roleId;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * Gets  $name value
     * @param $name
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Gets  $description value
     * @param $description
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Sets  $name value
     * @param $name
     * @return void
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Sets  $description value
     * @param $description
     * @return void
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * Selects all Role items
     * @return
     *   array The object having all Role items
     *    or string with the Exception details if error occured
     */
    public function selectAllRoles() {
        try {
            $daoSelect = new DaoSelect();
            return $daoSelect->selectAllRoles();
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Gets Role item
     * @param $roleId int  The table primary key
     * @return
     *   array The object with the given $roleId value
     */
    public function getRoleDetails($roleId) {
        try {
            $daoSelect = new DaoSelect();
            return $daoSelect->selectRoleDetails($roleId);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Inserts new Role entity
     * @param  $name string The entity  name field 
     * @param   $description string The entity  description field
     * @return
     *   boolean TRUE if insert successful, otherwise FALSE
     */
    public function insertNewRole($name, $description) {
        try {
            $daoInsert = new DaoInsert();
            return $daoInsert->insertNewRole($name, $description);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Updates Role item
     * @param  $roleId int The entity  roleId field 
     * @param  $name string The entity  name field 
     * @param  $description string The entity  description field 
     * @return
     *   boolean  TRUE if update successful, otherwise FALSE
     */
    public function updateRole($roleId, $name, $description) {
        try {
            $daoUpdate = new DaoUpdate();
            return $daoUpdate->updateRole($roleId, $name, $description);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Updates Role item  details

     * @param  $roleId int The entity  roleId field 
     * @param  $name string The entity  name field 
     * @param  $description string The entity  description field 
     * @return
     *   boolean TRUE if update successful, otherwise FALSE
     */
    public function updateRoleDetails($roleId, $name, $description) {
        try {
            $daoUpdate = new DaoUpdate();
            return $daoUpdate->updateRoleDetails($roleId, $name, $description);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Updates name of the Role entity
     * @param   $roleId int The entity  roleId field 
     * @param   $name string The entity  name field
     * @return
     *   boolean TRUE if update successful, otherwise FALSE
     */
    public function updateRoleName($roleId, $name) {
        try {
            $daoUpdate = new DaoUpdate();
            return $daoUpdate->updateRoleName($roleId, $name);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Updates description of the Role entity
     * @param   $roleId int The entity  roleId field 
     * @param   $description string The entity  description field
     * @return
     *   boolean TRUE if update successful, otherwise FALSE
     */
    public function updateRoleDescription($roleId, $description) {
        try {
            $daoUpdate = new DaoUpdate();
            return $daoUpdate->updateRoleDescription($roleId, $description);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Updates specific field of the Role item
     * @param    $fieldName string The field name
     * @param    $keyFieldValue int The primary key field value
     * @param    $newFieldValue string The new field value
     * @return
     *   boolean TRUE if update successful, otherwise FALSE
     */
    public function updateRoleEntityField($fieldName, $keyFieldValue, $newFieldValue) {
        try {
            switch ($fieldName) {

                case "name":
                    return $this->updateRoleName($keyFieldValue, $newFieldValue);

                case "description":
                    return $this->updateRoleDescription($keyFieldValue, $newFieldValue);
            }
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
        return true;
    }

    /**
     * Deletes Role item
     * @param  $roleId int  The table primary key
     * @return    boolean|mixed TRUE if delete successful, otherwise string with error message
     */
    public function deleteRole($roleId) {
        try {
            $daoDelete = new DaoDelete();
            return $daoDelete->deleteRole($roleId);
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

    /**
     * Deletes selected Role items
     * @param  $selectedItemsId array The List of primary keys item to be deleted
     * @return boolean|mixed TRUE if delete successful, otherwise string with error message
     */
    public function deleteSelectedRoles($selectedItemsId) {

        try {
            $daoDelete = new DaoDelete();
            if (!isset($selectedItemsId) || !is_array($selectedItemsId)) {
                return "Error: Invalid Parameters type for this method.";
            }
            foreach (array_values($selectedItemsId) as $itemId) {
                if ($daoDelete->deleteRole($itemId)) {
                    continue;
                } else {
                    return "error: Can not delete Role !";
                }
            }
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
        return true;
    }

    /**
     * Deletes all Role items
     * @return    boolean|mixed TRUE if delete successful, otherwise string with error message
     */
    public function deleteAllRoles() {
        try {
            $daoDelete = new DaoDelete();
            return $daoDelete->deleteAllRoles();
        } catch (ExceptionLogger $e) {
            return $e->getErrorMessage();
        }
    }

}
