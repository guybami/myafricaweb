<?php

   /**
   * IndexUIResource
   * @author     Guy Bami
   */
   class ExampleUIResource{
       
        public static function getOkBtnLabelText()
        {
            return "Confirmer";
        }

        public static function getSectionTitleLabelText()
        {
            return "Informations generales";
        }

        public static function getConfirmMessageLabelText()
        {
            return "Vous avez confirme le message";
        }

        public static function getDescriptionLabelText()
        {
            return '
                Marvin Gaye �tait comme Marvin Pentz Gay, Jr. � Washington, DC n�. Son p�re �tait un pr�tre
                Pentecostal Church of God, maison de pri�re, une section conservatrice de l\'Eglise de Dieu.
                Marvin Gaye avait un demi-fr�re a�n�, Michael Cooper, un vieux et une soeur plus jeune et un
                fr�re cadet, Frankie Gaye, qui �tait aussi un musicien lui-m�me plus tard.
            ';
        }

        public static function getOpeningDatesLabelText()
        {
            return "Heures Ouvertures";
        }
        
    }
	 
 