<?php

   /**
   * resource file for english UI culture
   * @author     Guy Bami
   */
   class ExampleUIResource{
       
        public static function getOkBtnLabelText()
        {
            return "Confirm";
        }

        public static function getSectionTitleLabelText()
        {
            return "General Informations.";
        }

        public static function getConfirmMessageLabelText()
        {
            return "You have confirmed the message";
        }

        public static function getDescriptionLabelText()
        {
            return '
                Marvin Gaye Marvin Pentz Gay was in when, Jr. Washington, DC born. His father was a priest of 
                the Pentecostal Church of God, House of Prayer, a conservative section of the Church of God. 
                Marvin Gaye had an older half-brother, Michael Cooper, an older and a younger sister and a younger brother, 
                Frankie Gaye, who himself later was also a musician.
            ';
        }

        public static function getOpeningDatesLabelText()
        {
            return "Opening Dates";
        }
        
    }
	 
 