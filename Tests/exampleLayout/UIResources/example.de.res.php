<?php

   /**
   * resource file for de UI culture
   * @author     Guy Bami
   */
class ExampleUIResource{
       
        public static function getOkBtnLabelText()
        {
            return "Best�tigen";
        }

        public static function getSectionTitleLabelText()
        {
            return "Informationen";
        }

        public static function getConfirmMessageLabelText()
        {
            return "Sie haben die Nachricht best�tigt";
        }

        public static function getDescriptionLabelText()
        {
            return '
                Marvin Gaye wurde als Marvin Pentz Gay, Jr. in Washington, D.C. geboren. Sein Vater war Priester 
                der Pfingstgemeinde Church of God, House of Prayer, einer konservativen Sektion der Church of God. 
                Marvin Gaye hatte einen �lteren Halbbruder, Michael Cooper, eine �ltere und eine j�ngere Schwester und einen 
                j�ngeren Bruder, Frankie Gaye, der selbst sp�ter ebenfalls Musiker wurde. 
            ';
        }

        public static function getOpeningDatesLabelText()
        {
            return "�ffnungszeiten";
        }
        
    }
 