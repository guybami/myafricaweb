
<?php


/**
 * This module was auto generated by GWatcho-Code generator
 * The Conference cotntroller entity class.
 * @author
 *    Guy Bami
 */


include_once 'BaseController.php';


class ConferenceController extends BaseController {
    private $conference = null;


    /**
     *Constructor of the Conference controller
     * @param $userAction string the user action
     * @param $userRoles array the user roles
     */
    function __construct($userAction, $userRoles = array()){
        parent::__construct($userAction, $userRoles);
        $this->conference = new Conference();
    }


    /**
     * Gets all Conference entities
     * @return mixed object having all Conference entities
     *    or string with the Exception details if error occured
     */
    public function getAllConferences(){

        $itemSperator = ",";
        $entitiesList = "";
        $resultObject = $this->conference->selectAllConferences();
        if(is_string($resultObject)){
            return Utils::formatJsonErrorMessage($resultObject);
        }
        else if(is_array($resultObject)){
            for ($i = 0; $i < count($resultObject); $i++)
            {
                $entitiesList .=
                '{"conferenceId":"'.$resultObject[$i]["conferenceId"].'"'
                .',"date":"'.$resultObject[$i]["date"].'"'
                .',"title":"'.$resultObject[$i]["title"].'"'
                .',"location":"'.$resultObject[$i]["location"].'"'
                .',"summary":"'.Utils::escapeJsonChars($resultObject[$i]["summary"]).'"'
                .'}';

                if ($i != count($resultObject) - 1) {
                    $entitiesList .= $itemSperator;
                }
            }
            // close json list
            $entitiesList = "[" . $entitiesList . "]";

        }
        return Utils::encodeSpecialCahrsForJson($entitiesList);
    }


    /**
     * Inserts new Conference entity
     * @param mixed $jsonData json object entity to insert
     * @return mixed  true if insertion was successful
     *    or string with the Exception details if error occured
     */
    public function insertNewConference($jsonData){

        // get json posted values from request
        $formJsonValues = json_decode($jsonData, true);
        $postbackData = "";
        $resultObject = null;
        // insert entity using model object
        $resultObject = $this->conference->insertNewConference(
                            $formJsonValues['date'] . " " . date("H:i:s")
                            ,$formJsonValues['title']
                            ,$formJsonValues['location']
                            ,$formJsonValues['summary']
        );

        if(is_bool($resultObject) || is_int($resultObject)){
            $postbackData = Utils::formatJsonMessage("insertedItemKey", $resultObject);
        }
        else if(is_string($resultObject)){
            //error occured
            $postbackData = Utils::formatJsonErrorMessage($resultObject);
        }
        else {
            //error occured
            $postbackData = Utils::formatJsonErrorMessage($resultObject);
        }
        return $postbackData;

    }


    /**
     * Updates Conference entity via POST request
     * @param int $customerId
     * @param string $updateMode
     * @param mixed $jsonData
     * @return mixed  true if update was successful
     *    or string with the Exception details if error occured
     */
    public function updateConference($conferenceId, $updateMode, $jsonData){

        // get json posted values from request
        $formJsonValues = json_decode($jsonData, true);
        $postbackData = "";
        $resultObject = null;
        if ($updateMode == "allFields") {
            // update all entity fields
            $resultObject = $this->conference->updateConferenceDetails(
                          $conferenceId
,$formJsonValues['date']
,$formJsonValues['title']
,$formJsonValues['location']
,$formJsonValues['summary']


            );
            if(is_bool($resultObject) && $resultObject == true){
                $postbackData = Utils::formatJsonResultMessage(Common::UPDATE_SUCCESSFUL);
            }
            if(is_string($resultObject)){
                //error occured
                $postbackData = Utils::formatJsonErrorMessage($resultObject);
            }

        }
        else if ($updateMode == "inlineUpdate") {
            // Update entity from the datagrid
            $fieldName = $formJsonValues['fieldName'];
            $keyFieldValue = $formJsonValues['entityKeyId'];
            $newFieldValue = $formJsonValues['newFieldValue'];
            $resultObject = $this->conference->updateConferenceEntityField($fieldName, $keyFieldValue, $newFieldValue);
            if(is_string($resultObject)){
                //error occured
                $postbackData = Utils::formatJsonErrorMessage($resultObject);
            }
            else if(is_bool($resultObject)){
                $postbackData = Utils::formatJsonResultMessage(Common::UPDATE_INLINE_SUCCESSFUL);
            }
        }
        return $postbackData;

    }


    /**
     * Deletes all selected entities via POST request
     * @param mixed $jsonData all entities Id to delete
     * @return mixed
     */
    public function deleteConferences($jsonData){

        // delete entity using model
        $resultObject = $this->conference->deleteSelectedConferences($jsonData);
        if(is_string($resultObject)){
            //error occured
            $postbackData = Utils::formatJsonErrorMessage($resultObject);
        }
        else if(is_bool($resultObject) && $resultObject == true){
            $postbackData = Utils::formatJsonResultMessage(Common::DELETE_SUCCESSFUL);
        }
        else {
            $postbackData = Utils::formatJsonErrorMessage($resultObject);
        }
        return $postbackData;

    }


    /**
     * Deletes all entities
     * @param mixed $jsonData all entities Id to delete
     * @return mixed
     */
    public function deleteAllConferences(){

        // delete entities using corresponding model
        $resultObject = $this->conference->deleteAllConferences();
        if(is_string($resultObject)){
            //error occured
            $postbackData = Utils::formatJsonErrorMessage($resultObject);
        }
        else if(is_bool($resultObject) && $resultObject == true){
            $postbackData = Utils::formatJsonResultMessage(Common::DELETE_SUCCESSFUL);
        }
        else {
            $postbackData = Utils::formatJsonErrorMessage($resultObject);
        }
        return $postbackData;

    }


    /**
     * Get dynamic page content
     * @return mixed
     */
    public function getDynamicPageContent() {

        $postbackData = Utils::formatJsonErrorMessage("Undefined Content");
        $resultObject = null;
        switch ($this->getUserAction()) {
            case "getAllItems":
                $postbackData = $this->getAllConferences();
                break;
            case "insertNewItem":
                $jsonData = array();
                if (isset($_POST['formValues'])) {
                    $jsonData = $_POST['formValues'][0];
                }
                $postbackData = $this->insertNewConference($jsonData);
                break;
            case "updateItem":
                $updateMode = "allFields";
                if (isset($_POST['updateMode'])) {
                    $updateMode = $_POST['updateMode'];
                }
                $conferenceId = $_POST['conferenceId'];
                $jsonData = $_POST['formValues'][0];
                $postbackData = $this->updateConference($conferenceId, $updateMode, $jsonData);
                break;
            case "deleteItem":
                $jsonData = array();
                if (isset($_POST['selectedIds'])) {
                    $jsonData = $_POST['selectedIds'];
                }
                $postbackData = $this->deleteConferences($jsonData);
                break;
            case "deleteAllItems":
                $postbackData = $this->deleteAllConferences();
                break;
            case "editDetails":
                $postbackData = "Undefined Content";
                if ( isset($_POST['conferenceId']) ) {
                    $conferenceId = $_POST['conferenceId'];
                    $resultObject = $this->conference->getConferenceDetails($conferenceId);
                }
                if(is_string($resultObject)){
                    $postbackData = Utils::formatJsonErrorMessage($resultObject);
                }
                else if(is_array($resultObject)){
                    $postbackData = Utils::convertArrayToJson($resultObject[0]);
                }
                break;
            case "addNewItem":
                //insert new entity
                $resultObject = Array('0' => Array(
                     date=>''
                    ,title=>''
                    ,location=>''
                    ,summary=>''
                   )
                );
                $postbackData = Utils::convertArrayToJson($resultObject[0]);
                break;
            case "viewDetails":
            case "cancelChanges":
                if ( isset($_POST['conferenceId']) ) {
                    $conferenceId = $_POST['conferenceId'];
                    $resultObject = $this->conference->getConferenceDetails($conferenceId);
                }
                else if(is_string($resultObject)){
                    $postbackData = Utils::formatJsonErrorMessage($resultObject);
                }
                else {
                    $postbackData = Utils::convertArrayToJson($resultObject[0]);
                    //print_r($resultObject[0]);
                }
                $postbackData = Utils::convertArrayToJson($resultObject[0]);
                break;
        }
        return $postbackData;

    }

}


$userAction = null;
if (isset($_POST) && isset($_POST['userAction'])) {
    $userAction = $_POST['userAction'];
}
if (!isset($userAction)) {
    echo "No action has been set";
}
else{
    //ob_start();
    $conferenceController = new ConferenceController($userAction);
    $conferenceController->setUserAction($userAction);
    $postbackContent = $conferenceController->getDynamicPageContent();
    // clear previous content
    //ob_flush();
    // send back content to client
    echo trim($postbackContent);
}


