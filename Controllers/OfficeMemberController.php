
<?php

/**
 * This module was auto generated by GWatcho-Code generator
 * The OfficeMember cotntroller entity class.
 * @author
 *    Guy Bami
 */


include_once 'BaseController.php';


class OfficeMemberController extends BaseController
{
    private $officeMember = null;
    /**
     *Constructor of the OfficeMember controller 
     * @param $userAction string the user action
     * @param $userRoles array the user roles 
     */
    
    function __construct($userAction, $userRoles = array())
    {
        parent::__construct($userAction, $userRoles);
        $this->officeMember = new OfficeMember();
    }
    
    
    /**
     * Gets all OfficeMember entities
     * @return mixed object having all OfficeMember entities
     *    or string with the Exception details if error occured
     */
    public function getAllOfficeMembers()
    {
        
        $itemSperator = ",";
        $entitiesList = "";
        $resultObject = $this->officeMember->selectAllOfficeMembers();
        if (is_string($resultObject)) {
            return Utils::formatJsonErrorMessage($resultObject);
        } else if (is_array($resultObject)) {
            for ($i = 0; $i < count($resultObject); $i++) {
                $entitiesList .= 
                        '{"officeMemberId":"'.$resultObject[$i]["officeMemberId"].'"'
                        .',"gender":"'.$resultObject[$i]["gender"].'"'
                        .',"name":"'.$resultObject[$i]["name"].'"'
                        .',"lastName":"'.$resultObject[$i]["lastName"].'"'
                        .',"email":"'.$resultObject[$i]["email"].'"'
                        .',"phoneNumber":"'.$resultObject[$i]["phoneNumber"].'"'
                        .',"zipCode":"'.$resultObject[$i]["zipCode"].'"'
                        .',"city":"'.$resultObject[$i]["city"].'"'
                        .',"address":"'.$resultObject[$i]["address"].'"'
                        .',"position":"'.$resultObject[$i]["position"].'"'
                        .'}';
                
                if ($i != count($resultObject) - 1) {
                    $entitiesList .= $itemSperator;
                }
            }
            // close json list
            $entitiesList = "[" . $entitiesList . "]";
            
        }
        return $entitiesList;
        
    }
    
    
    /**
     * Inserts new OfficeMember entity
     * @param mixed $jsonData json object entity to insert
     * @return mixed  true if insertion was successful
     *    or string with the Exception details if error occured
     */
    public function insertNewOfficeMember($jsonData)
    {
        
        // get json posted values from request
        $formJsonValues = json_decode($jsonData, true);
        $postbackData   = "";
        $resultObject   = null;
        // insert entity using model object
        $resultObject   = $this->officeMember->insertNewOfficeMember($formJsonValues['memberId'], $formJsonValues['position']);
        
        if (is_bool($resultObject) || is_int($resultObject)) {
            $postbackData = Utils::formatJsonMessage("insertedItemKey", $resultObject);
        } else if (is_string($resultObject)) {
            //error occured
            $postbackData = Utils::formatJsonErrorMessage($resultObject);
        } else {
            //error occured
            $postbackData = Utils::formatJsonErrorMessage($resultObject);
        }
        return $postbackData;
        
    }
    
    
    /**
     * Updates OfficeMember entity via POST request
     * @param int $customerId
     * @param string $updateMode
     * @param mixed $jsonData
     * @return mixed  true if update was successful
     *    or string with the Exception details if error occured
     */
    public function updateOfficeMember($officeMemberId, $updateMode, $jsonData)
    {
        
        // get json posted values from request
        $formJsonValues = json_decode($jsonData, true);
        $postbackData   = "";
        $resultObject   = null;
        if ($updateMode == "allFields") {
            // update all entity fields
            $resultObject = $this->officeMember->updateOfficeMemberDetails($officeMemberId, $formJsonValues['memberId'], $formJsonValues['position']);
            if (is_bool($resultObject)) {
                if ($resultObject == true) {
                    $postbackData = Utils::formatJsonResultMessage(Common::UPDATE_SUCCESSFUL);
                } else {
                    $postbackData = Utils::formatJsonResultMessage(Common::UPDATE_FAILED);
                }
            } else if (is_string($resultObject)) {
                //error occured
                $postbackData = Utils::formatJsonErrorMessage($resultObject);
            }
            
        } else if ($updateMode == "inlineUpdate") {
            // Update entity from the datagrid
            $fieldName     = $formJsonValues['fieldName'];
            $keyFieldValue = $formJsonValues['entityKeyId'];
            $newFieldValue = $formJsonValues['newFieldValue'];
            $resultObject  = $this->officeMember->updateOfficeMemberEntityField($fieldName, $keyFieldValue, $newFieldValue);
            if (is_string($resultObject)) {
                //error occured
                $postbackData = Utils::formatJsonErrorMessage($resultObject);
            } else if (is_bool($resultObject)) {
                $postbackData = Utils::formatJsonResultMessage(Common::UPDATE_INLINE_SUCCESSFUL);
            }
        }
        return $postbackData;
        
    }
    
    
    /**
     * Deletes all selected entities via POST request
     * @param mixed $jsonData all entities Id to delete
     * @return mixed
     */
    public function deleteOfficeMembers($jsonData)
    {
        
        // delete entity using model
        $resultObject = $this->officeMember->deleteSelectedOfficeMembers($jsonData);
        if (is_bool($resultObject)) {
            if ($resultObject == true) {
                $postbackData = Utils::formatJsonResultMessage(Common::DELETE_SUCCESSFUL);
            } else {
                $postbackData = Utils::formatJsonResultMessage(Common::DELETE_FAILED);
            }
        } else if (is_string($resultObject)) {
            //error occured
            $postbackData = Utils::formatJsonErrorMessage($resultObject);
        } else {
            $postbackData = Utils::formatJsonErrorMessage($resultObject);
        }
        return $postbackData;
        
    }
    
    
    /**
     * Deletes all entities
     * @param mixed $jsonData all entities Id to delete
     * @return mixed
     */
    public function deleteAllOfficeMembers()
    {
        
        // delete entities using corresponding model
        $resultObject = $this->officeMember->deleteAllOfficeMembers();
        if (is_bool($resultObject)) {
            if ($resultObject == true) {
                $postbackData = Utils::formatJsonResultMessage(Common::DELETE_SUCCESSFUL);
            } else {
                $postbackData = Utils::formatJsonResultMessage(Common::DELETE_FAILED);
            }
        } else if (is_string($resultObject)) {
            //error occured
            $postbackData = Utils::formatJsonErrorMessage($resultObject);
        } else {
            $postbackData = Utils::formatJsonErrorMessage($resultObject);
        }
        return $postbackData;
        
    }
    
    
    /**
     * Get dynamic page content
     * @return mixed
     */
    public function getDynamicPageContent()
    {
        
        $postbackData = "Undefined Content";
        $resultObject = null;
        switch ($this->getUserAction()) {
            case "getAllItems":
                $postbackData = $this->getAllOfficeMembers();
                break;
            case "addNewItem":
            case "insertNewItem":
                $jsonData = array();
                if (isset($_POST['formValues'])) {
                    $jsonData = $_POST['formValues'][0];
                }
                $postbackData = $this->insertNewOfficeMember($jsonData);
                break;
            case "updateItem":
                $updateMode = "allFields";
                if (isset($_POST['updateMode'])) {
                    $updateMode = $_POST['updateMode'];
                }
                $officeMemberId = $_POST['officeMemberId'];
                $jsonData       = $_POST['formValues'][0];
                $postbackData   = $this->updateOfficeMember($officeMemberId, $updateMode, $jsonData);
                break;
            case "deleteItem":
                $jsonData = array();
                if (isset($_POST['selectedIds'])) {
                    $jsonData = $_POST['selectedIds'];
                }
                $postbackData = $this->deleteOfficeMembers($jsonData);
                break;
            case "deleteAllItems":
                $postbackData = $this->deleteAllOfficeMembers();
                break;
            case "editDetails":
                $postbackData = "Undefined Content";
                if (isset($_POST['officeMemberId'])) {
                    $officeMemberId = $_POST['officeMemberId'];
                    $resultObject   = $this->officeMember->getOfficeMemberDetails($officeMemberId);
                }
                if (is_string($resultObject)) {
                    $postbackData = Utils::formatJsonErrorMessage($resultObject);
                } else if (is_array($resultObject)) {
                    $postbackData = Utils::convertArrayToJson($resultObject[0]);
                }
                break;
            case "addNewItem":
                //insert new entity
                $resultObject = Array(
                    '0' => Array(
                        memberId => '',
                        position => ''
                        
                        
                    )
                );
                $postbackData = Utils::convertArrayToJson($resultObject[0]);
                break;
            case "viewDetails":
            case "cancelChanges":
                if (isset($_POST['officeMemberId'])) {
                    $officeMemberId = $_POST['officeMemberId'];
                    $resultObject   = $this->officeMember->getOfficeMemberDetails($officeMemberId);
                }
                if (is_string($resultObject)) {
                    $postbackData = Utils::formatJsonErrorMessage($resultObject);
                } else if (is_array($resultObject)) {
                    $postbackData = Utils::convertArrayToJson($resultObject[0]);
                }
                break;
        }
        return $postbackData;
        
    }
    
}


$userAction = null;
if (isset($_POST) && isset($_POST['userAction'])) {
    $userAction = $_POST['userAction'];
}
if (!isset($userAction)) {
    echo "No action has been set";
} else {
    $officeMemberController = new OfficeMemberController($userAction);
    $officeMemberController->setUserAction($userAction);
    $postbackContent = $officeMemberController->getDynamicPageContent();
    
    // send back content to client
    echo $postbackContent;
}

